package br.com.otgmobile.segurancacolaborativa.gps;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;


public class DLGps {

	private static DLLocationListener listener;

	public static void addGpsObserver(DLGpsObserver observer, Context context){
		getListenerInstance(context).addObserver(observer);
	}

	public static void removeGpsObserver(DLGpsObserver observer, Context context){
		getListenerInstance(context).removeObserver(observer);
		if(listener.isTimeToTurnOff()){
			LocationManager locManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
			locManager.removeUpdates(listener);
			locManager = null;
			listener = null;
		}
	}

	private static DLLocationListener getListenerInstance(Context context){
		if ( listener == null ) {
			final LocationManager locManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
			listener = new DLLocationListener();
			locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 600, 1000, listener);
			locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 600, 1000, listener);
		}
		
		return listener;
	}

	public static Location getLastLocation(Context context){
		return getListenerInstance(context).getLastLocation();
	}

	public static int getLastStatus(Context context){
		return getListenerInstance(context).getLastStatus();
	}

	public static void turnGPSOn(Context context){
		String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

		if(!provider.contains(LocationManager.GPS_PROVIDER)){
			context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		}
	}
}
