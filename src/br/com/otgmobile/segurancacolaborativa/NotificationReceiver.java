package br.com.otgmobile.segurancacolaborativa;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import br.com.otgmobile.segurancacolaborativa.activity.AlarmeActivity;
import br.com.otgmobile.segurancacolaborativa.model.AlarmWrraper;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;

public class NotificationReceiver extends BroadcastReceiver {
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;
	Context ctx;

	@Override
	public void onReceive(Context context, Intent intent) {
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
		ctx = context;
		String messageType = gcm.getMessageType(intent);
		if (!GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)
				&& !GoogleCloudMessaging.MESSAGE_TYPE_DELETED
						.equals(messageType)) {
			String alarmeJson = intent.getExtras().getString("message_text");
			Alarme alarme = getAlarmeFromJson(alarmeJson);
			sendNotification(alarme);
		}
		setResultCode(Activity.RESULT_OK);
	}

	private Alarme getAlarmeFromJson(String json) {
		Gson gson = new Gson();
		AlarmWrraper obj = gson.fromJson(json, AlarmWrraper.class);
		Alarme alarme = obj.getAlarme();
		return alarme;
	}

	// Put the GCM message into a notification and post it.
	private void sendNotification(Alarme alarme) {
		String sensor = alarme.getSensor().getLocaizacao();
		mNotificationManager = (NotificationManager) ctx
				.getSystemService(Context.NOTIFICATION_SERVICE);

		PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0,
				new Intent(ctx, AlarmeActivity.class), 0);

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				ctx).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(ctx.getText(R.string.title_notification_alarme))
				.setStyle(new NotificationCompat.BigTextStyle().bigText(sensor))
				.setContentText(sensor);

		mBuilder.setContentIntent(contentIntent);
		mNotificationManager.notify(alarme.getId().intValue(), mBuilder.build());
	}

}
