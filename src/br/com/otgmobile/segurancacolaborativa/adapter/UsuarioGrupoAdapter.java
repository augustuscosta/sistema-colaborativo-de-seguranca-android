package br.com.otgmobile.segurancacolaborativa.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.listeners.ExcluirUsuarioListener;
import br.com.otgmobile.segurancacolaborativa.listeners.UsuarioExcluir;
import br.com.otgmobile.segurancacolaborativa.model.GrupoColaborativo;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;

public class UsuarioGrupoAdapter  extends BaseAdapter{
	
	private Activity context;
	private List<Usuario> usuarios;
	private UsuarioExcluir usuarioExcluir;
	private GrupoColaborativo grupo;
	private String email;
	
	public UsuarioGrupoAdapter(Activity context,List<Usuario> usuarios, GrupoColaborativo grupo, String email, UsuarioExcluir usuarioExcluir){
		this.context =context;
		this.usuarios = usuarios;
		this.grupo = grupo;
		this.usuarioExcluir = usuarioExcluir;
		this.email = email;
	}

	@Override
	public int getCount() {
		return usuarios == null ? 0 : usuarios.size();
	}

	@Override
	public Object getItem(int position) {
		return usuarios == null ? 0 : usuarios.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		UsuarioGrupoRowHolder holder;
		Usuario usuario = usuarios.get(position);
		
		if(row == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.usuario_grupo_row, null);
			holder = new UsuarioGrupoRowHolder();
			holder.usuarioTextView = (TextView) row.findViewById(R.id.usuario);
			holder.linha = (View) row.findViewById(R.id.linha);
			holder.remover = (Button) row.findViewById(R.id.remover);
		}else{
			holder = (UsuarioGrupoRowHolder) row.getTag();
		}
		
		holder.remover.setOnClickListener(new ExcluirUsuarioListener(position, usuarioExcluir, row));
		
		row.setTag(holder);
		holder.drawRow(usuario, grupo, email,context);
		return row;
	}


}
