package br.com.otgmobile.segurancacolaborativa.adapter;

import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.model.Comentario;

public class AtendimentoRowHolder {

	public TextView userNameTextView;
	public TextView comentTextView;
	
	public void drawRow(Comentario obj){
		if(obj == null) return;
		
		if(obj.getUsuario()!= null && obj.getUsuario().getName()!= null){
			userNameTextView.setText(obj.getUsuario().getName());
		}else{
			userNameTextView.setText("");
		}
		if(obj.getMensagem()!= null){
			comentTextView.setText(obj.getMensagem());
		}else{
			comentTextView.setText("");
		}
		
		
	}
	
}
