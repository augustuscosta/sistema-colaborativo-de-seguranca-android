package br.com.otgmobile.segurancacolaborativa.adapter;

import android.app.Activity;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.model.Convite;

public class ConviteRowHolder {

	public TextView grupoTextView;
	public TextView remetenteTextView;
	public Button remover;
	public Button adicionar;
	public LinearLayout conviteDetailContainer;
	
	public void drawRow(Convite obj,Activity context){
		if(obj == null) return;
		
		grupoTextView.setText(obj.getGrupoNome());
		remetenteTextView.setText(obj.getNomeRemetente());
	}

}
