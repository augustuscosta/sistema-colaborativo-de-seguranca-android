package br.com.otgmobile.segurancacolaborativa.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.listeners.AdicionarConviteListener;
import br.com.otgmobile.segurancacolaborativa.listeners.ConviteAdicionar;
import br.com.otgmobile.segurancacolaborativa.listeners.ConviteExcluir;
import br.com.otgmobile.segurancacolaborativa.listeners.ExcluirConviteListener;
import br.com.otgmobile.segurancacolaborativa.model.Convite;

public class ConviteAdapter  extends BaseAdapter{
	
	private Activity context;
	private List<Convite> convites;
	private ConviteExcluir conviteExcluir;
	private ConviteAdicionar conviteConvidar;
	
	public ConviteAdapter(Activity context,List<Convite> convites,ConviteExcluir conviteExcluir, ConviteAdicionar conviteConvidar){
		this.context =context;
		this.convites = convites;
		this.conviteConvidar = conviteConvidar;
		this.conviteExcluir = conviteExcluir;
	}

	@Override
	public int getCount() {
		return convites == null ? 0 : convites.size();
	}

	@Override
	public Object getItem(int position) {
		return convites == null ? 0 : convites.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		ConviteRowHolder holder;
		Convite convite = convites.get(position);
		
		if(row == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.convite_row, null);
			holder = new ConviteRowHolder();
			holder.grupoTextView = (TextView) row.findViewById(R.id.grupo);
			holder.remetenteTextView = (TextView) row.findViewById(R.id.remetente);
			holder.remover = (Button) row.findViewById(R.id.remover);
			holder.adicionar = (Button) row.findViewById(R.id.adicionar);
			holder.conviteDetailContainer = (LinearLayout) row.findViewById(R.id.conviteDetailContainer);
		}else{
			holder = (ConviteRowHolder) row.getTag();
		}
		
		holder.remover.setOnClickListener(new ExcluirConviteListener(position, conviteExcluir, row));
		holder.adicionar.setOnClickListener(new AdicionarConviteListener(position, conviteConvidar, row));
		
		row.setTag(holder);
		holder.drawRow(convite, context);
		return row;
	}


}
