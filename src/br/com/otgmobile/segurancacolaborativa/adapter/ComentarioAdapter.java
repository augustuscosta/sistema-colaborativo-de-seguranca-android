package br.com.otgmobile.segurancacolaborativa.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.model.Comentario;

public class ComentarioAdapter extends BaseAdapter {

	private List<Comentario> comentarios;
	private Activity context;
	
	public ComentarioAdapter(List<Comentario> comentarios, Activity context) {
		this.comentarios = comentarios;
		this.context = context;
	}

	@Override
	public int getCount() {
		return comentarios == null ?0 : comentarios.size();
	}

	@Override
	public Object getItem(int position) {
		return comentarios == null ?null : comentarios.get(position); 
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		AtendimentoRowHolder rowHolder;
		
		if(row == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.atendimento_row, null);
			rowHolder = new AtendimentoRowHolder();
			rowHolder.userNameTextView = (TextView) row.findViewById(R.id.usernameText);
			rowHolder.comentTextView = (TextView) row.findViewById(R.id.comentText);
		}else{
			rowHolder = (AtendimentoRowHolder) row.getTag();
		}
		
		rowHolder.drawRow(comentarios.get(position));
		row.setTag(rowHolder);
		return row;
	}

}
