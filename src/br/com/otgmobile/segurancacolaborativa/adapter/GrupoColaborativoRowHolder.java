package br.com.otgmobile.segurancacolaborativa.adapter;

import android.app.Activity;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.model.GrupoColaborativo;

public class GrupoColaborativoRowHolder {

	public TextView grupoTextView;
	public TextView emailTextView;
	public Button remover;
	public Button convidar;
	public LinearLayout grupoDetailContainer;
	
	public void drawRow(GrupoColaborativo obj,Activity context){
		if(obj == null) return;
		
		grupoTextView.setText(obj.getNome());
		emailTextView.setText(obj.getEmailDono());
	}

}
