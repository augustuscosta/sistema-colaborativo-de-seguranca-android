package br.com.otgmobile.segurancacolaborativa.adapter;

import android.app.Activity;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.util.AppHelper;

public class AlarmeRowHolder {

	public TextView dateTextView;
	public TextView timeTextView;
	public TextView casaView;
	public TextView userNameTextView;
	public TextView originTextView;
	public Button locate;
	public Button preview;
	public LinearLayout alarmeDetailContainer;
	
	public void drawRow(Alarme obj,Activity context){
		if(obj == null) return;
		
		if(obj.getData() != null){
			dateTextView.setText(AppHelper.getInstance().formateDate(context, obj.getData()));
			timeTextView.setText(AppHelper.getInstance().formateTime(context, obj.getData()));
		}else{
			dateTextView.setText("");
			timeTextView.setText("");
		}
		
		if(obj.getSensor() !=null && obj.getSensor().getLocaizacao() != null){
			casaView.setText(obj.getSensor().getLocaizacao());
		}else{
			casaView.setText("");
		}
		
		if(obj.getSensor() != null && obj.getSensor().getTipoSensor() != null && obj.getSensor().getTipoSensor().getNome()!= null){
			originTextView.setText(obj.getSensor().getTipoSensor().getNome());
		}else{
			originTextView.setText("");
		}
		
		if(obj.getSensor() != null && obj.getSensor().getUsuario() != null && obj.getSensor().getUsuario().getName() != null){
			userNameTextView.setText(obj.getSensor().getUsuario().getName());
		}else{
			userNameTextView.setText("");
			
		}
		
		
		
	}

}
