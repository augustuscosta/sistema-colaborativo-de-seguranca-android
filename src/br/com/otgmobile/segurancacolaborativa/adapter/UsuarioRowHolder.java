package br.com.otgmobile.segurancacolaborativa.adapter;

import android.app.Activity;
import android.widget.Button;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.model.Atendimento;
import br.com.otgmobile.segurancacolaborativa.model.AtendimentoStatus;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;

public class UsuarioRowHolder {
	
	public TextView usuarioTextView;
	
	public Button indictor;
	
	public void drawRow(Usuario obj,Activity context, Alarme alarme){
		if(obj == null) return;
		
		if(obj.getName() != null){
			usuarioTextView.setText(obj.getName());
		}
		
		if(obj.getAtendimento() == null || obj.getAtendimento().isEmpty()){
			indictor.setBackground(context.getResources().getDrawable(R.drawable.livre_indicador));
		}else{
			for(Atendimento atendimento:obj.getAtendimento()){
				if(atendimento.getAlarmeId().equals(alarme.getId())){
					if(AtendimentoStatus.fromValue(atendimento.getStatus())!= null && AtendimentoStatus.fromValue(atendimento.getStatus()) == AtendimentoStatus.indisponivel ){
						indictor.setBackground(context.getResources().getDrawable(R.drawable.indisponivel_indicador));
						break;
					}
					if(AtendimentoStatus.fromValue(atendimento.getStatus())!= null && AtendimentoStatus.fromValue(atendimento.getStatus()) == AtendimentoStatus.atendendo ){
						indictor.setBackground(context.getResources().getDrawable(R.drawable.ocupado_indicador));
						break;
					}					
				}
			}
		}
	}
	

}
