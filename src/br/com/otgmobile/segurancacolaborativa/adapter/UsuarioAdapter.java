package br.com.otgmobile.segurancacolaborativa.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;

public class UsuarioAdapter extends BaseAdapter {
	
	private Activity context;
	private List<Usuario> usuarios;
	private Alarme alarme;
	
	public UsuarioAdapter(Activity context,List<Usuario> usuarios,Alarme alarme){
		this.usuarios = usuarios;
		this.context = context;
		this.alarme = alarme;
	}

	@Override
	public int getCount() {
		return usuarios == null ?0 : usuarios.size() ;
	}

	@Override
	public Object getItem(int position) {
		return usuarios == null ?null: usuarios.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		UsuarioRowHolder rowHolder;
		if(row == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.usuario_row, null);
			rowHolder = new UsuarioRowHolder();
			rowHolder.indictor = (Button) row.findViewById(R.id.status_indicator);
			rowHolder.usuarioTextView = (TextView) row.findViewById(R.id.name_textView);
		}else{
			rowHolder = (UsuarioRowHolder) row.getTag();
 		}
		row.setTag(rowHolder);
		rowHolder.drawRow(usuarios.get(position), context,alarme);
		return row;
	}

}
