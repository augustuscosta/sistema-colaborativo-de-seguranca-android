package br.com.otgmobile.segurancacolaborativa.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.model.GrupoColaborativo;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;

public class UsuarioGrupoRowHolder {

	public TextView usuarioTextView;
	public Button remover;
	public View linha;
	
	public void drawRow(Usuario obj, GrupoColaborativo grupo, String email, Activity context){
		if(obj == null) return;
		
		usuarioTextView.setText(obj.getName());
		
		if( email.equals(grupo.getEmailDono()) && !email.equals(obj.getEmail())){
			linha.setVisibility(View.VISIBLE);
			remover.setVisibility(View.VISIBLE);
		}
	}

}
