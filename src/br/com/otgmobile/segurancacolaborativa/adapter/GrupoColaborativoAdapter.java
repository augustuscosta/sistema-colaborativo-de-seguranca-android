package br.com.otgmobile.segurancacolaborativa.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.listeners.ConvidarGrupoListener;
import br.com.otgmobile.segurancacolaborativa.listeners.ExcluirGrupoListener;
import br.com.otgmobile.segurancacolaborativa.listeners.GrupoConvidar;
import br.com.otgmobile.segurancacolaborativa.listeners.GrupoExcluir;
import br.com.otgmobile.segurancacolaborativa.listeners.GrupoPreview;
import br.com.otgmobile.segurancacolaborativa.listeners.PreviewGrupoListenter;
import br.com.otgmobile.segurancacolaborativa.model.GrupoColaborativo;

public class GrupoColaborativoAdapter  extends BaseAdapter{
	
	private Activity context;
	private List<GrupoColaborativo> grupos;
	private GrupoExcluir grupoExcluir;
	private GrupoConvidar grupoConvidar;
	private GrupoPreview grupoPreview;
	
	public GrupoColaborativoAdapter(Activity context,List<GrupoColaborativo> grupos,GrupoExcluir grupoExcluir, GrupoConvidar grupoConvidar, GrupoPreview grupoPreview){
		this.context =context;
		this.grupos = grupos;
		this.grupoConvidar = grupoConvidar;
		this.grupoExcluir = grupoExcluir;
		this.grupoPreview = grupoPreview;
	}

	@Override
	public int getCount() {
		return grupos == null ? 0 : grupos.size();
	}

	@Override
	public Object getItem(int position) {
		return grupos == null ? 0 : grupos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		GrupoColaborativoRowHolder holder;
		GrupoColaborativo grupo = grupos.get(position);
		
		if(row == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.grupo_row, null);
			holder = new GrupoColaborativoRowHolder();
			holder.grupoTextView = (TextView) row.findViewById(R.id.grupo);
			holder.emailTextView = (TextView) row.findViewById(R.id.email);
			holder.remover = (Button) row.findViewById(R.id.remover);
			holder.convidar = (Button) row.findViewById(R.id.convidar);
			holder.grupoDetailContainer = (LinearLayout) row.findViewById(R.id.grupoDetailContainer);
		}else{
			holder = (GrupoColaborativoRowHolder) row.getTag();
		}
		
		holder.remover.setOnClickListener(new ExcluirGrupoListener(position, grupoExcluir, row));
		holder.convidar.setOnClickListener(new ConvidarGrupoListener(position, grupoConvidar, row));
		holder.grupoDetailContainer.setOnClickListener(new PreviewGrupoListenter(position, grupoPreview, row));
		
		row.setTag(holder);
		holder.drawRow(grupo, context);
		return row;
	}


}
