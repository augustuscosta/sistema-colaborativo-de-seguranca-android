package br.com.otgmobile.segurancacolaborativa.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.listeners.AlarmeComentater;
import br.com.otgmobile.segurancacolaborativa.listeners.AlarmeLocater;
import br.com.otgmobile.segurancacolaborativa.listeners.AlarmePreview;
import br.com.otgmobile.segurancacolaborativa.listeners.ComentAlarmListenter;
import br.com.otgmobile.segurancacolaborativa.listeners.LocateAlarmListenter;
import br.com.otgmobile.segurancacolaborativa.listeners.PreviewAlarmListenter;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;

public class AlarmeAdapter  extends BaseAdapter{
	
	private Activity context;
	private List<Alarme> alarmes;
	private AlarmeLocater locater;
	private AlarmePreview previewer;
	private AlarmeComentater comenter;
	
	public AlarmeAdapter(Activity context,List<Alarme> alarmes,AlarmeLocater locater,AlarmePreview previewer,AlarmeComentater comenter){
		this.context =context;
		this.alarmes = alarmes;
		this.locater = locater;
		this.previewer = previewer;
		this.comenter = comenter;
	}

	@Override
	public int getCount() {
		return alarmes == null ? 0 : alarmes.size();
	}

	@Override
	public Object getItem(int position) {
		return alarmes == null ? 0 : alarmes.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		AlarmeRowHolder holder;
		Alarme alarme = alarmes.get(position);
		
		if(row == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.alarme_row, null);
			holder = new AlarmeRowHolder();
			holder.dateTextView = (TextView) row.findViewById(R.id.date);
			holder.timeTextView = (TextView) row.findViewById(R.id.time);
			holder.casaView = (TextView) row.findViewById(R.id.location);
			holder.originTextView = (TextView) row.findViewById(R.id.originName);
			holder.userNameTextView = (TextView) row.findViewById(R.id.username);
			holder.locate = (Button) row.findViewById(R.id.geocode);
			holder.preview = (Button) row.findViewById(R.id.details);
			holder.alarmeDetailContainer = (LinearLayout) row.findViewById(R.id.alarmeDetailContainer);
		}else{
			holder = (AlarmeRowHolder) row.getTag();
		}
		holder.locate.setOnClickListener(new LocateAlarmListenter(position, locater,row));
		holder.preview.setOnClickListener(new ComentAlarmListenter(position, comenter, row));
		holder.alarmeDetailContainer.setOnClickListener(new PreviewAlarmListenter(position, previewer,row));
		
		row.setTag(holder);
		holder.drawRow(alarme, context);
		return row;
	}


}
