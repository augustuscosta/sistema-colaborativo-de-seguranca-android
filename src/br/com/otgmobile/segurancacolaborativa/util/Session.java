package br.com.otgmobile.segurancacolaborativa.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;

public class Session {
	

	public static final String PREFS 						 = "SESSION_PREFS";
	public static final String SERVER_PREFS_KEY 			 = "SERVER_PREFS_KEY";
	public static final String TOKEN_PREFS_KEY 				 = "REST_TOKEN_PREFS_KEY";
	public static final String REGISTRATION_ID_KEY 			 = "REGISTRATION_ID_KEY";
	public static final String SENDER_ID_KEY 			 	 = "SENDER_ID_KEY";
	public static final String SERVER_REGISTRED_KEY 		 = "SERVER_REGISTRED_KEY";
	public static final String GCM_REGISTRED_KEY		 	 = "GCM_REGISTRED_KEY";
	public static final String USER_EMAIL			 	 	 = "USER_EMAIL";

	private static SharedPreferences settings;
 
	private static SharedPreferences getSharedPreferencesInstance(Context context){
		if(settings == null){
			settings = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		}
		return settings;
	}
	
	public static String getDeviceId(Context context){
		TelephonyManager TelephonyMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		String szImei = TelephonyMgr.getDeviceId();
		return szImei;
	}
	
	public static void setServer(String server,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SERVER_PREFS_KEY, server);
		editor.commit();
	}
	
	public static String getServer(Context context){				
		return getSharedPreferencesInstance(context).getString(SERVER_PREFS_KEY, "http://10.0.3.2:3000/");
	}
	
	public static String getToken(Context context){
		String token = getSharedPreferencesInstance(context).getString(TOKEN_PREFS_KEY, null);
		if(token == null || token.length() < 1 ){
			return null;
		}
		return token;
	}
	
	public static void setToken(String token,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(TOKEN_PREFS_KEY, token);
		editor.commit();
	}
	
	public static void clearSession(Context context){
		Session.setToken("", context);
	}
	
	public static void setRegistrationId(String registrationId,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(REGISTRATION_ID_KEY, registrationId);
		editor.commit();
	}
	
	public static String getRegistrationId(Context context){				
		return getSharedPreferencesInstance(context).getString(REGISTRATION_ID_KEY, null);
	}
	
	public static void setSenderId(String senderId,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SENDER_ID_KEY, senderId);
		editor.commit();
	}
	
	public static String getSenderId(Context context){				
		return getSharedPreferencesInstance(context).getString(SENDER_ID_KEY, "769241877806");
	}
	
	public static void setServerRegistred(Boolean registred,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(SERVER_REGISTRED_KEY, registred);
		editor.commit();
	}
	
	public static Boolean getServerRegistred(Context context){				
		return getSharedPreferencesInstance(context).getBoolean(SERVER_REGISTRED_KEY, false);
	}
	
	public static void setGCMRegistred(Boolean registred,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(GCM_REGISTRED_KEY, registred);
		editor.commit();
	}
	
	public static Boolean getGCMRegistred(Context context){				
		return getSharedPreferencesInstance(context).getBoolean(GCM_REGISTRED_KEY, false);
	}
	
	public static String getEmailFromPreferences(Context context){
		String email = getSharedPreferencesInstance(context).getString(USER_EMAIL, null);
		if(email == null || email.length() < 1 ){
			return null;
		}
		return email;
	}
	
	public static void setEmailToPreferences(Context context, String email){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(USER_EMAIL, email);
		editor.commit();
	}
}
