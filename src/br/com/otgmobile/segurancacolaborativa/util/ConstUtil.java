package br.com.otgmobile.segurancacolaborativa.util;


public class ConstUtil {

	public static final String COLON = ":";
	public static final String HIFEN = "-";
	public static final String SLASH = "/";
	public static final String TIME = "T";
	public static final String DOUBLESPACES ="  ";

	// Server Messages
	
	public static final int INVALID_TOKEN	 = 302;
	public static final String SERIALIZABLE_NAME = "serializable_object";
	public static final String PAGE = "page";
	
}
