package br.com.otgmobile.segurancacolaborativa.cloud;

import java.sql.SQLException;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.com.otgmobile.segurancacolaborativa.database.DatabaseAccessProvider;
import br.com.otgmobile.segurancacolaborativa.model.Convite;
import br.com.otgmobile.segurancacolaborativa.util.ConstUtil;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;
import br.com.otgmobile.segurancacolaborativa.util.Session;

import com.google.gson.Gson;
import com.j256.ormlite.table.TableUtils;

public class ConviteCloud extends RestClient {
	
	
	private final static String PATH ="convites";
	private Context context;
	private DatabaseAccessProvider provider;
	
	public ConviteCloud(Context context){
		this.context = context;
	}
	
	public int createConvite(Convite convite) throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		Gson gson = new Gson();
		setUrl(addSlashIfNeeded(url)+PATH+JSON_TYPE);
		String conviteJson = gson.toJson(convite, Convite.class);
		setToken(Session.getToken(context));
		addParam("convite", conviteJson);
		execute(RequestMethod.POST);
		return getResponseCode();
	}
	
	public int updateConvite(Convite convite) throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		Gson gson = new Gson();
		setUrl(addSlashIfNeeded(url)+PATH+ConstUtil.SLASH+convite.getId()+JSON_TYPE);
		String conviteJson = gson.toJson(convite, Convite.class);
		setToken(Session.getToken(context));
		addParam("convite", conviteJson);
		execute(RequestMethod.PUT);
		return getResponseCode();
	}
	
	public void removeConvite(Convite convite) throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		setUrl(addSlashIfNeeded(url)+PATH+ConstUtil.SLASH+convite.getId()+JSON_TYPE);
		setToken(Session.getToken(context));
		execute(RequestMethod.DELETE);
	}
	
	public void  getAllConvites() throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		setUrl(addSlashIfNeeded(url)+PATH+JSON_TYPE);
		setToken(Session.getToken(context));
		execute(RequestMethod.GET);
		if(getResponseCode() == HttpStatus.SC_OK){
			clearTables();
			getConvitesFromResponse();
		}
		
	}
	
	private void clearTables(){
		try {
			TableUtils.clearTable(provider().getConnectionDataSource(), Convite.class);
		} catch (SQLException e) {
			Log.e("Erro limpando Tabela", e.getMessage());
		}
	}

	private void getConvitesFromResponse() throws Exception {
		JSONArray jsonArray = getJsonArrayFromResponse();
		Gson gson = new Gson();
		for (int i = 0; i < jsonArray.length(); i++) {
			persistConviteFromJSon(jsonArray, gson, i);
		}
	}

	private void persistConviteFromJSon(JSONArray jsonArray, Gson gson, int i) {
		try{
			
		JSONObject object = jsonArray.getJSONObject(i);
		Convite convite = gson.fromJson(object.getString("convite").toString(), Convite.class);
		provider().conviteDao().createOrUpdate(convite);
		}catch(Exception e){
			LogUtil.e("erro ao persistir o convite da nuvem", e);
		}
	}

	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(context);
		}
		
		return provider;
	}
}
