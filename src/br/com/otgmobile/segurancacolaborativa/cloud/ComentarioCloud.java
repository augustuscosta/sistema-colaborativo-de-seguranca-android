package br.com.otgmobile.segurancacolaborativa.cloud;

import java.sql.SQLException;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import br.com.otgmobile.segurancacolaborativa.database.DatabaseAccessProvider;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.model.Comentario;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;
import br.com.otgmobile.segurancacolaborativa.util.Session;

import com.google.gson.Gson;

public class ComentarioCloud extends RestClient {
	
	private static final String rootObject = "comentario";
	private Context context;
	private DatabaseAccessProvider provider;
	private static final String PATH ="comentarios";

	public ComentarioCloud(Context context) {
		this.context = context;
	}
	
	public int getComentarios() throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		setUrl(addSlashIfNeeded(url)+PATH+JSON_TYPE);
		setToken(Session.getToken(context));
		execute(RequestMethod.GET);
		if(getResponseCode() == HttpStatus.SC_OK){
			getComentariosFromResponse();
		}
		return getResponseCode();
	}
	
	public int createComentario(Comentario comentario,Alarme alarme){
		cleanParams();
		String url = Session.getServer(context);
		setUrl(addSlashIfNeeded(url)+PATH+JSON_TYPE);
		Gson gson = new Gson();
		addParam(rootObject, gson.toJson(comentario));
		setToken(Session.getToken(context));
		try {
			execute(RequestMethod.POST);
		} catch (Exception e) {
			LogUtil.e("",e);
		}
		if(getResponseCode() ==HttpStatus.SC_CREATED){
			try {
				getComentarioFromResponse(alarme);
			} catch (Exception e) {
				LogUtil.e("",e);
			}
		}
		return getResponseCode();
	}

	private void getComentarioFromResponse(Alarme alarme) throws Exception {
		JSONObject jsonObject = getJsonObjectFromResponse();
		Gson gson = new Gson();
		Comentario comentario = gson.fromJson(jsonObject.getString(rootObject).toString(), Comentario.class);
		if(comentario!= null){
			comentario.setAlarme(alarme);
			persistComentarioWithAlignedObjects(comentario);
			
		}
	}

	private void getComentariosFromResponse() throws Exception {
		JSONArray jsonArray = getJsonArrayFromResponse();
		Gson gson = new Gson();
		for (int i = 0; i < jsonArray.length(); i++) {
			Comentario comentario = gson.fromJson(jsonArray.getJSONObject(i).getString(rootObject).toString(), Comentario.class);
			persistComentarioWithAlignedObjects(comentario);
		}
	}

	private void persistComentarioWithAlignedObjects(Comentario comentario)throws SQLException {
		provider().comentarioDao().createOrUpdate(comentario);
		Usuario usuario = comentario.getUsuario();
		if(usuario!= null){
			provider.persitUsuarioWithGrupo(usuario);
		}
	}

	private DatabaseAccessProvider provider() {
		if(provider == null){
			provider = new DatabaseAccessProvider(context);
		}
		return provider;
	}
	

}
