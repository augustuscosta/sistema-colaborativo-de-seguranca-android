package br.com.otgmobile.segurancacolaborativa.cloud;

import java.sql.SQLException;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.com.otgmobile.segurancacolaborativa.database.DatabaseAccessProvider;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.model.Atendimento;
import br.com.otgmobile.segurancacolaborativa.model.Comentario;
import br.com.otgmobile.segurancacolaborativa.model.Sensor;
import br.com.otgmobile.segurancacolaborativa.model.TipoSensor;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;
import br.com.otgmobile.segurancacolaborativa.util.ConstUtil;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;
import br.com.otgmobile.segurancacolaborativa.util.Session;

import com.google.gson.Gson;
import com.j256.ormlite.table.TableUtils;

public class AlarmeCloud extends RestClient {
	
	
	private final static String PATH ="alarmes";
	private Context context;
	private DatabaseAccessProvider provider;
	
	public AlarmeCloud(Context context){
		this.context = context;
	}
	
	public int createAlarme(Alarme alarme) throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		Gson gson = new Gson();
		setUrl(addSlashIfNeeded(url)+PATH+JSON_TYPE);
		String alarmeJson = gson.toJson(alarme, Alarme.class);
		setToken(Session.getToken(context));
		addParam("alarme", alarmeJson);
		execute(RequestMethod.POST);
		if (getResponseCode() == HttpStatus.SC_CREATED) {
			getAlarmeFromResponse(); 
		}
		
		return getResponseCode();
	}
	
	public void  getAllAlarmes() throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		setUrl(addSlashIfNeeded(url)+PATH+	JSON_TYPE);
		setToken(Session.getToken(context));
		execute(RequestMethod.GET);
		if(getResponseCode() == HttpStatus.SC_OK){
			clearTables();
			getAlarmesFromResponse();
		}
		
	}
	
	private void clearTables(){
		try {
			TableUtils.clearTable(provider().getConnectionDataSource(), Comentario.class);
		} catch (SQLException e) {
			Log.e("Erro limpando Tabela", e.getMessage());
		}
		try {
			TableUtils.clearTable(provider().getConnectionDataSource(), Atendimento.class);
		} catch (SQLException e) {
			Log.e("Erro limpando Tabela", e.getMessage());
		}
		try {
			TableUtils.clearTable(provider().getConnectionDataSource(), Sensor.class);
		} catch (SQLException e) {
			Log.e("Erro limpando Tabela", e.getMessage());
		}
		try {
			TableUtils.clearTable(provider().getConnectionDataSource(), Alarme.class);
		} catch (SQLException e) {
			Log.e("Erro limpando Tabela", e.getMessage());
		}
	}
	
	public int  getupadatedAlarme(Alarme alarme) throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		setUrl(addSlashIfNeeded(url)+PATH+ConstUtil.SLASH+alarme.getId()+JSON_TYPE);
		setToken(Session.getToken(context));
		execute(RequestMethod.GET);
		boolean updated =getResponseCode() == HttpStatus.SC_OK;
		if(updated){
			getAlarmeFromResponse();
		}
		
		return getResponseCode();
		
	}

	private void getAlarmesFromResponse() throws Exception {
		JSONArray jsonArray = getJsonArrayFromResponse();
		Gson gson = new Gson();
		for (int i = 0; i < jsonArray.length(); i++) {
			persistAlarmeFromJSon(jsonArray, gson, i);
		}
	}

	private void persistAlarmeFromJSon(JSONArray jsonArray, Gson gson, int i) {
		try {

			JSONObject object = jsonArray.getJSONObject(i);
			Alarme alarme = gson.fromJson(
					object.getString("alarme").toString(), Alarme.class);
			provider().alarmeDao().createOrUpdate(alarme);
			persistAlignedObjects(alarme);
		} catch (Exception e) {
			LogUtil.e("erro ao persistir o alarme da nuvem", e);
		}
	}

	private void persistAlignedObjects(Alarme alarme) throws SQLException {
		Sensor sensor = alarme.getSensor();
		if(sensor!= null){
			provider().sensorDao().createOrUpdate(sensor);
			TipoSensor tipoSensor =  sensor.getTipoSensor();
			Usuario usuario = sensor.getUsuario();
			if(tipoSensor != null){
				provider().tipoSensorDao().createOrUpdate(tipoSensor);
			}
			if(usuario!=null){
				provider().persitUsuarioWithGrupo(usuario);
			}
		}
		if(alarme.getComentarios() != null){
			for(Comentario comentario: alarme.getComentarios()){
				comentario.setAlarme(alarme);
				Usuario usuario = provider().usuarioDao().queryForId(comentario.getUsuarioID());
				if(comentario.getUsuario() == null && usuario != null) comentario.setUsuario(usuario);
				provider().comentarioDao().createOrUpdate(comentario);
			}
		}
		
		if(alarme.getAtendimentos()!= null){
			for(Atendimento atendimento : alarme.getAtendimentos()){
				atendimento.setAlarme(alarme);
				Usuario usuario = provider().usuarioDao().queryForId(atendimento.getUsuarioID());
				if(atendimento.getUsuario() == null && usuario != null) atendimento.setUsuario(usuario);
				provider().atendimentoDao().createOrUpdate(atendimento);
			}
		}
	}

	private void getAlarmeFromResponse() throws Exception {
		JSONObject jsonObject = getJsonObjectFromResponse();
		if (jsonObject != null) {
			Alarme toReturn;
			Gson gson = new Gson();
			toReturn = gson.fromJson(jsonObject.getString("alarme").toString(),Alarme.class);
			provider().alarmeDao().createOrUpdate(toReturn);
			persistAlignedObjects(toReturn);
		}
	}

	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(context);
		}
		
		return provider;
	}
}
