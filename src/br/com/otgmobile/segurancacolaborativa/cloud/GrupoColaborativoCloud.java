package br.com.otgmobile.segurancacolaborativa.cloud;

import java.sql.SQLException;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import br.com.otgmobile.segurancacolaborativa.database.DatabaseAccessProvider;
import br.com.otgmobile.segurancacolaborativa.model.GrupoColaborativo;
import br.com.otgmobile.segurancacolaborativa.model.GrupoUsuario;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;
import br.com.otgmobile.segurancacolaborativa.util.ConstUtil;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;
import br.com.otgmobile.segurancacolaborativa.util.Session;

import com.google.gson.Gson;
import com.j256.ormlite.table.TableUtils;

public class GrupoColaborativoCloud extends RestClient {
	
	
	private final static String PATH ="grupo_colaborativos";
	private final static String PATH_REMOVE_USUARIO ="remove_usuario";
	private Context context;
	private DatabaseAccessProvider provider;
	
	public GrupoColaborativoCloud(Context context){
		this.context = context;
	}
	
	public int createGrupo(GrupoColaborativo grupo) throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		Gson gson = new Gson();
		setUrl(addSlashIfNeeded(url)+PATH+JSON_TYPE);
		String grupoJson = gson.toJson(grupo, GrupoColaborativo.class);
		setToken(Session.getToken(context));
		addParam("grupo_colaborativo", grupoJson);
		execute(RequestMethod.POST);
		if (getResponseCode() == HttpStatus.SC_CREATED) {
			getGrupoFromResponse(); 
		}
		
		return getResponseCode();
	}
	
	public void removeGrupo(GrupoColaborativo grupo) throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		setUrl(addSlashIfNeeded(url)+PATH+ConstUtil.SLASH+grupo.getId()+JSON_TYPE);
		setToken(Session.getToken(context));
		execute(RequestMethod.DELETE);
	}
	
	public void removerUsuarioDoGrupo(GrupoColaborativo grupo, Usuario usuario) throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		Gson gson = new Gson();
		setUrl(addSlashIfNeeded(url)+PATH+ "/"+ grupo.getId()+ "/"+  PATH_REMOVE_USUARIO+JSON_TYPE);
		grupo.setUsuarios(null);
		grupo.setGrupoUsuarios(null);
		usuario.setGruposColaborativos(null);
		usuario.setGrupoUsuarios(null);
		usuario.setSensores(null);
		String grupoJson = gson.toJson(grupo, GrupoColaborativo.class);
		String usuarioJson = gson.toJson(usuario, Usuario.class);
		setToken(Session.getToken(context));
		addParam("grupo_colaborativo", grupoJson);
		addParam("usuario", usuarioJson);
		execute(RequestMethod.POST);
	}
	
	public int  getGrupo(GrupoColaborativo grupo) throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		setUrl(addSlashIfNeeded(url)+PATH+ConstUtil.SLASH+grupo.getId()+JSON_TYPE);
		setToken(Session.getToken(context));
		execute(RequestMethod.GET);
		boolean updated =getResponseCode() == HttpStatus.SC_OK;
		if(updated){
			getGrupoFromResponse();
		}
		
		return getResponseCode();
		
	}
	
	public void  getAllGrupos() throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		setUrl(addSlashIfNeeded(url)+PATH+JSON_TYPE);
		setToken(Session.getToken(context));
		execute(RequestMethod.GET);
		if(getResponseCode() == HttpStatus.SC_OK){
			clearTables();
			getGruposFromResponse();
		}
		
	}
	
	private void clearTables(){
		try {
			TableUtils.clearTable(provider().getConnectionDataSource(), GrupoUsuario.class);
		} catch (SQLException e) {
			Log.e("Erro limpando Tabela", e.getMessage());
		}
		try {
			TableUtils.clearTable(provider().getConnectionDataSource(), GrupoColaborativo.class);
		} catch (SQLException e) {
			Log.e("Erro limpando Tabela", e.getMessage());
		}
		try {
			TableUtils.clearTable(provider().getConnectionDataSource(), Usuario.class);
		} catch (SQLException e) {
			Log.e("Erro limpando Tabela", e.getMessage());
		}
	}

	private void getGruposFromResponse() throws Exception {
		JSONArray jsonArray = getJsonArrayFromResponse();
		Gson gson = new Gson();
		for (int i = 0; i < jsonArray.length(); i++) {
			persistGrupoFromJSon(jsonArray, gson, i);
		}
	}

	private void persistGrupoFromJSon(JSONArray jsonArray, Gson gson, int i) {
		try{
			
		JSONObject object = jsonArray.getJSONObject(i);
		GrupoColaborativo grupo = gson.fromJson(object.getString("grupo_colaborativo").toString(), GrupoColaborativo.class);
		provider().grupoColaborativoDao().createOrUpdate(grupo);
		persistAlignedObjects(grupo);
		}catch(Exception e){
			LogUtil.e("erro ao persistir o grupo da nuvem", e);
		}
	}

	private void persistAlignedObjects(GrupoColaborativo grupo) throws SQLException {
		if(grupo.getUsuarios()!= null){
			for(Usuario usuario : grupo.getUsuarios()){
				provider().usuarioDao().createOrUpdate(usuario);
				provider().persistGrupoUSuario(usuario, grupo);
			}
		}
	}

	private void getGrupoFromResponse() throws Exception {
		JSONObject jsonObject = getJsonObjectFromResponse();
		if (jsonObject != null) {
			GrupoColaborativo toReturn;
			Gson gson = new Gson();
			toReturn = gson.fromJson(jsonObject.getString("grupo_colaborativo").toString(),GrupoColaborativo.class);
			persistAlignedObjects(toReturn);
		}
	}

	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(context);
		}
		
		return provider;
	}
}
