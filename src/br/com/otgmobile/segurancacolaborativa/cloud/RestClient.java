package br.com.otgmobile.segurancacolaborativa.cloud;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.otgmobile.segurancacolaborativa.util.ConstUtil;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;


public class RestClient {
	
	private static final int REQUEST_TIME_OUT = 5000;
	public enum RequestMethod {GET, POST,PUT, DELETE};
	
	protected static final String HEADER_COOKIE = "Cookie";
	private static final String COCKIE_PARAMENTER_RAILS_SESSION = "_sistema_colaborativo_seguranca_session";
	protected static final String HEADER_SET_COOKIE = "Set-Cookie";	
	protected static final String JSON_TYPE = ".json";
	
	
	
    private ArrayList <NameValuePair> params = new ArrayList<NameValuePair>();
    private ArrayList <NameValuePair> headers = new ArrayList<NameValuePair>();
 
    private String url;
    private String token;
 
    public  int responseCode;
    public String message;
 
    private String response;
    protected HttpResponse httpResponse;
	
    
    public void cleanParams() {
    	params.clear();
    	headers.clear();
    }
 
    public String getResponse() {
        return response;
    }
 
    public String getErrorMessage() {
        return message;
    }
 
    public int getResponseCode() {
        return responseCode;
    }
    
    public String getExceptionConstForMessage() {
    	String toReturn = "";
    	if(message != null) {
    		if(message.indexOf("%") > 0 && message.lastIndexOf("%") > 0) {
    			toReturn = message.substring(message.indexOf("%") + 1, message.lastIndexOf("%"));
    		} else {
    			toReturn = message;
    		}
    	}
    	
    	return toReturn;
    }
    
    public String addSlashIfNeeded(String url) {
    	if(url.lastIndexOf(ConstUtil.SLASH) != url.length() -1) {
    		return url += ConstUtil.SLASH;
    	}
    	
    	return url;
    }
 
    public void addParam(String name, String value) {
        params.add(new BasicNameValuePair(name, value));
    }
 
    public void addHeader(String name, String value) {
        headers.add(new BasicNameValuePair(name, value));
    }
 
    public void execute(RequestMethod method) throws Exception {
    	setHeaderAuthorization();
        switch(method) {
            case GET:
            {
            	doGet(url);
                break;
            }
            case DELETE:
            {
            	doDelete(url);
                break;
            }
            case POST:
            {
                doPost(url);
                break;
            }
            
            case PUT:
            {
                doPut(url);
                break;
            }
		default:
			break;
        }
    }
    
    /**
     * Seta o token de autentica????o
     */
    private void setHeaderAuthorization() {
    	if (token == null) {
    		return;
    	}
    	addHeader(HEADER_COOKIE, token);
    }

	/**
     * @param url
     * @throws Exception
     * @method doGet
     */
    public void doGet(String url) throws Exception {
    	if (url == null || url.trim().length() == 0) {
    		return;
    	}
    	
    	StringBuilder urlWithParams = new StringBuilder(url);
    	urlWithParams = parseParamsToUrl(urlWithParams);
        
        HttpGet request = new HttpGet(urlWithParams.toString());
        
        //add headers
        for(NameValuePair h : headers) {
            request.addHeader(h.getName(), h.getValue());
        }
        
        executeRequest(request, urlWithParams.toString());
    }
    
    /**
     * @param url
     * @throws Exception
     * @method doDelete
     */
    public void doDelete(String url) throws Exception {
    	if (url == null || url.trim().length() == 0) {
    		return;
    	}
    	
    	StringBuilder urlWithParams = new StringBuilder(url);
    	urlWithParams = parseParamsToUrl(urlWithParams);
        
        HttpDelete request = new HttpDelete(urlWithParams.toString());
        
        //add headers
        for(NameValuePair h : headers) {
            request.addHeader(h.getName(), h.getValue());
        }
        
        executeRequest(request, urlWithParams.toString());
    }
    
	/**
     * @param url
     * @throws Exception
     * @method doPost
     */
    public void doPost(String url) throws Exception {
    	if (url == null || url.trim().length() == 0) {
    		return;
    	}
    	
    	
    	StringBuilder urlWithParams = new StringBuilder(url);
    	
    	HttpPost request = new HttpPost(urlWithParams.toString());
    	 
        //add headers
        for(NameValuePair h : headers) {
            request.addHeader(h.getName(), h.getValue());
        }

        if(!params.isEmpty()) {
        	UrlEncodedFormEntity  entity = new UrlEncodedFormEntity(params, HTTP.UTF_8);
        	entity.setContentType("application/x-www-form-urlencoded");
            request.setEntity(entity);
        }

        executeRequest(request, url);
    }
    
    public void doPut(String url) throws Exception {
    	if (url == null || url.trim().length() == 0) {
    		return;
    	}
    	
    	StringBuilder urlWithParams = new StringBuilder(url);
    	
    	HttpPut request = new HttpPut(urlWithParams.toString());
    	 
        //add headers
        for(NameValuePair h : headers) {
            request.addHeader(h.getName(), h.getValue());
        }

        if(!params.isEmpty()) {
            request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
        }

        executeRequest(request, url);
    }
    
    private StringBuilder parseParamsToUrl(StringBuilder urlWithParams) throws UnsupportedEncodingException {
    	if(!params.isEmpty()) {
            for(NameValuePair p : params) {
            	String paramName = URLEncoder.encode(p.getName(), HTTP.UTF_8);
            	urlWithParams.append(ConstUtil.SLASH).append(paramName);
                
                if (p.getValue() == null) {
                	continue;
                }
                
                urlWithParams.append(ConstUtil.SLASH).append(URLEncoder.encode(p.getValue(), HTTP.UTF_8));
            }
        }
    	
    	return urlWithParams;
	}
    
    public JSONArray getJsonArrayFromResponse() throws JSONException { 
    	if(response == null) {
    		return null;
    	}
    	
    	return new JSONArray(response);
    }
    
    public JSONObject getJsonObjectFromResponse() throws JSONException { 
    	if(response == null) {
    		return null;
    	}
    	
    	return new JSONObject(response);
    }
    
    public void executeRequest(HttpUriRequest request, String url){
    	HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(request.getParams(), REQUEST_TIME_OUT);
        HttpConnectionParams.setSoTimeout(request.getParams(), REQUEST_TIME_OUT);
        httpResponse = null;
        
        try {
            httpResponse = client.execute(request);
            responseCode = httpResponse.getStatusLine().getStatusCode();
            message = httpResponse.getStatusLine().getReasonPhrase();
            HttpEntity entity = httpResponse.getEntity();
            
            if (entity != null) {
            	InputStream instream = entity.getContent();
            	
            	try {
            		response = convertStreamToString(instream);
            	} finally {
            		instream.close();
            		client.getConnectionManager().shutdown();
            	}
            }
 
        } catch (ClientProtocolException e)  {
            client.getConnectionManager().shutdown();
            message = e.getLocalizedMessage();
            LogUtil.e(message, e);
        } catch (IOException e) {
            client.getConnectionManager().shutdown();
            message = e.getLocalizedMessage();
            LogUtil.e(message, e);
        }
    }
    
    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
 
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
            	sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        return sb.toString();
    }
    
	public boolean isValidRequest(int responseCode) {
		
		if (responseCode == HttpStatus.SC_OK) {
			return true;
		}

		return false;
	}
    
    // Get and Sets
	public ArrayList<NameValuePair> getParams() {
		return params;
	}

	public void setParams(ArrayList<NameValuePair> params) {
		this.params = params;
	}

	public ArrayList<NameValuePair> getHeaders() {
		return headers;
	}

	public void setHeaders(ArrayList<NameValuePair> headers) {
		this.headers = headers;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
	/**
	 * @return Token if request if success otherwise null.
	 */
	public String retrieveToken() {
		if (httpResponse == null) {
			// opss nao exist response algo errado.
			return null;
		}
		
		Header[] headers = httpResponse.getHeaders(HEADER_SET_COOKIE);
		if (headers == null) {
			// opss nao tem token 
			return null;
		}
		
		String token = null;
		for (Header header : headers) {
			if (header != null && header.getValue().contains(COCKIE_PARAMENTER_RAILS_SESSION)) {
				token = header.getValue();
			}
		}
		
		return token;
    }
    
}
