package br.com.otgmobile.segurancacolaborativa.cloud;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import br.com.otgmobile.segurancacolaborativa.database.DatabaseAccessProvider;
import br.com.otgmobile.segurancacolaborativa.model.Sensor;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;
import br.com.otgmobile.segurancacolaborativa.util.Session;

import com.google.gson.Gson;

public class SensorCloud extends RestClient {
	private Context context;
	private DatabaseAccessProvider provider;
	private static String PATH = "sensores";
	
	public SensorCloud(Context context) {
		this.context = context;
	}
	
	public void getSensorsFromCloud() throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		setUrl(addSlashIfNeeded(url)+PATH+	JSON_TYPE);
		setToken(Session.getToken(context));
		execute(RequestMethod.GET);
		if(getResponseCode() == HttpStatus.SC_OK){
			getSensoresFromResponse();
		}
	}

	
	private void getSensoresFromResponse() throws Exception {
		JSONArray jsonArray = getJsonArrayFromResponse();
		Gson gson = new Gson();
		for (int i = 0; i < jsonArray.length(); i++) {
			persistSesnroesFromJSon(jsonArray, gson, i);
		}
		
	}

	private void persistSesnroesFromJSon(JSONArray jsonArray, Gson gson, int i) {
		try{
		JSONObject object = jsonArray.getJSONObject(i);
		Sensor alarme = gson.fromJson(object.getString("alarme").toString(), Sensor.class);
		provider().sensorDao().createOrUpdate(alarme);
		}catch(Exception e){
			LogUtil.e("erro ao persistir o alarme da nuvem", e);
		}
		
	}

	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(context);
		}
		
		return provider;
	}
}
