package br.com.otgmobile.segurancacolaborativa.cloud;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import android.content.Context;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;
import br.com.otgmobile.segurancacolaborativa.util.Session;

public class RegistrationCloud extends RestClient {
	
	
	private final static String PATH ="usuarios";
	private Context context;
	
	public RegistrationCloud(Context context){
		this.context = context;
	}
	
	public int createUsuario(Usuario usuario) throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		JSONObject object = new JSONObject();
		object.accumulate("name", usuario.getName());
		object.accumulate("email", usuario.getEmail());
		object.accumulate("telefone", usuario.getTelefone());
		object.accumulate("password", usuario.getPassword());
		object.accumulate("password_confirmation", usuario.getPasswordConfirmation());
		JSONObject root = new JSONObject();
		root.put("usuario", object);
		createUser(url, root);
		return responseCode;
	}
	
	private void createUser(String url, JSONObject root)throws Exception {
		HttpPost httpPost = new HttpPost(url + PATH+JSON_TYPE);
		StringEntity entity = new StringEntity(root.toString(), HTTP.UTF_8);
		entity.setContentType("application/json");
		httpPost.setEntity(entity);
		HttpClient client = new DefaultHttpClient();
		httpResponse = client.execute(httpPost);
        responseCode = httpResponse.getStatusLine().getStatusCode();
        message = httpResponse.getStatusLine().getReasonPhrase();
		String token = retrieveToken();
		Session.setToken(token, context);
	}
}
