package br.com.otgmobile.segurancacolaborativa.cloud;

import android.content.Context;
import br.com.otgmobile.segurancacolaborativa.util.Session;

public class RegistrationIdCloud extends RestClient{

	private static final String REGISTER_PATH = "/usuarios/register_device";
	private static final String UNREGISTER_PATH = "/usuarios/unregister_device";
	private Context context;
	
	public RegistrationIdCloud(Context context){
		this.context = context;
	}
	
	public void register(String registrationId) throws Exception{
		cleanParams();
		String url = Session.getServer(context) + REGISTER_PATH;
		setUrl(url);
		setToken(Session.getToken(context));
		addParam("device", registrationId);
		execute(RequestMethod.POST);
	}
	
	public void unregister() throws Exception{
		cleanParams();
		String url = Session.getServer(context) + UNREGISTER_PATH;
		setUrl(url);
		setToken(Session.getToken(context));
		execute(RequestMethod.POST);
	}

}
