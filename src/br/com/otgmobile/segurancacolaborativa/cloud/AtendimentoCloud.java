package br.com.otgmobile.segurancacolaborativa.cloud;

import org.apache.http.HttpStatus;
import org.json.JSONObject;

import android.content.Context;
import br.com.otgmobile.segurancacolaborativa.database.DatabaseAccessProvider;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.model.Atendimento;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;
import br.com.otgmobile.segurancacolaborativa.util.ConstUtil;
import br.com.otgmobile.segurancacolaborativa.util.Session;

import com.google.gson.Gson;

public class AtendimentoCloud extends RestClient {

	private static final String ROOT_OBJECT = "atendimento";
	private static final String PATH = "atendimentos";
	private Context context;
	private DatabaseAccessProvider provider;
	
	public AtendimentoCloud(Context context) {
		this.context = context;
	}
	
	public int createAtendimento(Atendimento atendimento,Alarme alarme) throws Exception{
		String url = Session.getServer(context);
		Gson gson = new Gson();
		setUrl(addSlashIfNeeded(url)+PATH+JSON_TYPE);
		String atendimentoJson = gson.toJson(atendimento, Atendimento.class);
		setToken(Session.getToken(context));
		addParam(ROOT_OBJECT, atendimentoJson);
		execute(RequestMethod.POST);
		if(getResponseCode() == HttpStatus.SC_CREATED){
			getAtendimentoFromResponse(alarme);
		}
		return getResponseCode();
		
	}

	private void getAtendimentoFromResponse(Alarme alarme) throws Exception {
		JSONObject jsonObject = getJsonObjectFromResponse();
		if(jsonObject!= null){
			Atendimento toReturn;
			Gson gson = new Gson();
			toReturn = gson.fromJson(jsonObject.getString(ROOT_OBJECT).toString(),Atendimento.class);
			persistAtendimento(toReturn,alarme);
		}
		
	}

	private void persistAtendimento(Atendimento toReturn, Alarme alarme) throws Exception{
		Usuario usuario = toReturn.getUsuario();
		if(usuario!=null){
			provider().persitUsuarioWithGrupo(usuario);
		}
		toReturn.setAlarme(alarme);
		provider.atendimentoDao().createOrUpdate(toReturn);
	}
	
	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(context);
		}
		
		return provider;
	}

	public int updateAtendimento(Atendimento atendimento, Alarme alarme) throws Exception {
		String url = Session.getServer(context);
		Gson gson = new Gson();
		setUrl(addSlashIfNeeded(url)+PATH+ConstUtil.SLASH+atendimento.getId()+JSON_TYPE);
		atendimento.setUsuario(null);
		String atendimentoJson = gson.toJson(atendimento, Atendimento.class);
		setToken(Session.getToken(context));
		addParam(ROOT_OBJECT, atendimentoJson);
		execute(RequestMethod.PUT);
		if(getResponseCode() == HttpStatus.SC_CREATED){
			getAtendimentoFromResponse(alarme);
		}
		return getResponseCode();
	}

}
