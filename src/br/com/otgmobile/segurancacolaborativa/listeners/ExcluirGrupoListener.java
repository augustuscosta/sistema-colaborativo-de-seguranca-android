package br.com.otgmobile.segurancacolaborativa.listeners;

import android.view.View;
import android.view.View.OnClickListener;

public class ExcluirGrupoListener implements OnClickListener {
	
	GrupoExcluir handler;
	View v;
	int position;

	public ExcluirGrupoListener(int position,GrupoExcluir handler,View v) {
		this.position = position;
		this.handler = handler;
		this.v = v;
	}

	@Override
	public void onClick(View arg0) {
		handler.excluirGrupo(position);
	}

}
