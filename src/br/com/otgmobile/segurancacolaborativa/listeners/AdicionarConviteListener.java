package br.com.otgmobile.segurancacolaborativa.listeners;

import android.view.View;
import android.view.View.OnClickListener;

public class AdicionarConviteListener implements OnClickListener {
	
	ConviteAdicionar handler;
	View v;
	int position;

	public AdicionarConviteListener(int position,ConviteAdicionar handler,View v) {
		this.position = position;
		this.handler = handler;
		this.v = v;
	}

	@Override
	public void onClick(View arg0) {
		handler.adicionarConvite(position);
	}

}
