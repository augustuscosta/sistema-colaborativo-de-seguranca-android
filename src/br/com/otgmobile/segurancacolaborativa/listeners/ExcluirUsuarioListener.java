package br.com.otgmobile.segurancacolaborativa.listeners;

import android.view.View;
import android.view.View.OnClickListener;

public class ExcluirUsuarioListener implements OnClickListener {
	
	UsuarioExcluir handler;
	View v;
	int position;

	public ExcluirUsuarioListener(int position,UsuarioExcluir handler,View v) {
		this.position = position;
		this.handler = handler;
		this.v = v;
	}

	@Override
	public void onClick(View arg0) {
		handler.excluir(position);
	}

}
