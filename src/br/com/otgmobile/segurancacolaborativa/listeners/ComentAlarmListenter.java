package br.com.otgmobile.segurancacolaborativa.listeners;

import android.view.View;
import android.view.View.OnClickListener;

public class ComentAlarmListenter implements OnClickListener {
	
	AlarmeComentater handler;
	View v;
	int position;

	public ComentAlarmListenter(int position,AlarmeComentater handler,View v) {
		this.position = position;
		this.handler = handler;
		this.v = v;
	}

	@Override
	public void onClick(View arg0) {
		handler.comentAlarm(position);
	}

}
