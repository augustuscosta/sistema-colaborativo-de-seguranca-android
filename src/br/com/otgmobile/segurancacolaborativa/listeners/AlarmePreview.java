package br.com.otgmobile.segurancacolaborativa.listeners;

public interface AlarmePreview {
	void viewAlarmDetails(int position);
}
