package br.com.otgmobile.segurancacolaborativa.listeners;

public interface GrupoExcluir {

	void excluirGrupo(int position);
}
