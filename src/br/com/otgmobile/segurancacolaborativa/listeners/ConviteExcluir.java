package br.com.otgmobile.segurancacolaborativa.listeners;

public interface ConviteExcluir {

	void excluirConvite(int position);
}
