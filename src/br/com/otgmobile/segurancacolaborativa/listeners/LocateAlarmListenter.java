package br.com.otgmobile.segurancacolaborativa.listeners;

import android.view.View;
import android.view.View.OnClickListener;

public class LocateAlarmListenter implements OnClickListener {
	
	AlarmeLocater handler;
	View v;
	int position;

	public LocateAlarmListenter(int position,AlarmeLocater handler,View v) {
		this.position = position;
		this.handler = handler;
		this.v = v;
	}

	@Override
	public void onClick(View arg0) {
		handler.locateAlarme(position);
	}

}
