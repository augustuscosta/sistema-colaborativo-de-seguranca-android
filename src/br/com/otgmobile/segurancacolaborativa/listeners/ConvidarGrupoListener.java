package br.com.otgmobile.segurancacolaborativa.listeners;

import android.view.View;
import android.view.View.OnClickListener;

public class ConvidarGrupoListener implements OnClickListener {
	
	GrupoConvidar handler;
	View v;
	int position;

	public ConvidarGrupoListener(int position,GrupoConvidar handler,View v) {
		this.position = position;
		this.handler = handler;
		this.v = v;
	}

	@Override
	public void onClick(View arg0) {
		handler.convidarParaGrupo(position);
	}

}
