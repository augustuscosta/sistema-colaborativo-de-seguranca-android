package br.com.otgmobile.segurancacolaborativa.listeners;

import android.view.View;
import android.view.View.OnClickListener;

public class ExcluirConviteListener implements OnClickListener {
	
	ConviteExcluir handler;
	View v;
	int position;

	public ExcluirConviteListener(int position,ConviteExcluir handler,View v) {
		this.position = position;
		this.handler = handler;
		this.v = v;
	}

	@Override
	public void onClick(View arg0) {
		handler.excluirConvite(position);
	}

}
