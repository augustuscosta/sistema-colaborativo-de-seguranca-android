package br.com.otgmobile.segurancacolaborativa.listeners;

public interface UsuarioExcluir {

	void excluir(int position);
}
