package br.com.otgmobile.segurancacolaborativa.listeners;

import android.view.View;
import android.view.View.OnClickListener;

public class PreviewGrupoListenter implements OnClickListener {
	
	GrupoPreview handler;
	View v;
	int position;

	public PreviewGrupoListenter(int position,GrupoPreview handler,View v) {
		this.position = position;
		this.handler = handler;
		this.v = v;
	}

	@Override
	public void onClick(View arg0) {
		handler.viewGrupoDetails(position);
	}

}
