package br.com.otgmobile.segurancacolaborativa.listeners;

import android.view.View;
import android.view.View.OnClickListener;

public class PreviewAlarmListenter implements OnClickListener {
	
	AlarmePreview handler;
	View v;
	int position;

	public PreviewAlarmListenter(int position,AlarmePreview handler,View v) {
		this.position = position;
		this.handler = handler;
		this.v = v;
	}

	@Override
	public void onClick(View arg0) {
		handler.viewAlarmDetails(position);
	}

}
