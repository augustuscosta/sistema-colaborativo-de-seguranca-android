package br.com.otgmobile.segurancacolaborativa.listeners;

public interface GrupoConvidar {

	void convidarParaGrupo(int position);
}
