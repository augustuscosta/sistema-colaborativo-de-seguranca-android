package br.com.otgmobile.segurancacolaborativa.fragments;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import org.apache.http.HttpStatus;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.adapter.ConviteAdapter;
import br.com.otgmobile.segurancacolaborativa.cloud.ConviteCloud;
import br.com.otgmobile.segurancacolaborativa.database.DatabaseAccessProvider;
import br.com.otgmobile.segurancacolaborativa.listeners.ConviteAdicionar;
import br.com.otgmobile.segurancacolaborativa.listeners.ConviteExcluir;
import br.com.otgmobile.segurancacolaborativa.model.Convite;
import br.com.otgmobile.segurancacolaborativa.util.AppHelper;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;

import com.actionbarsherlock.app.SherlockListFragment;

public class ConviteListFragment extends SherlockListFragment implements ConviteExcluir, ConviteAdicionar, OnRefreshListener{
	
	public final static String TAG ="conviteslistFragment";
	private List<Convite> convites;
	ConviteAdapter adapter;
	DatabaseAccessProvider provider;
	private static GetAllConvitesTask getAllConvitesTask;
	private SwipeRefreshLayout swipeLayout;
	private ConviteCloud conviteCloud;
	private RemoverConviteTask removerConviteTask;
	private SendConviteTask sendConviteTask;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_convite_list, container, false);
		swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.conviteList);
		swipeLayout.setOnRefreshListener(this);
		return view;
	}
	
	
	@Override
	public void onResume() {
		executeGetConvitesTask();
		super.onResume();
	}
	

	@Override public void onRefresh() {
        new Handler().post(new Runnable() {
            @Override public void run() {
            	executeGetConvitesTask();
                swipeLayout.setRefreshing(false);
            }
        });
    }
	
	public void populateConvites() {
		try {
			convites = provider().conviteDao().queryBuilder().orderBy("id", false).query();
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar os convites", e);
			convites = Collections.emptyList();
		}
		adapter = new ConviteAdapter(getActivity(), convites, this, this);
		setListAdapter(adapter);
	}
	
	
	@Override
	public void onDestroyView() {
		convites = null;
		provider = null;
		super.onDestroyView();
	}
	
	@Override
	public void adicionarConvite(int position) {
		stopSendConviteTask();
	    SendConviteTask task = new SendConviteTask(getActivity(), convites.get(position));
	    task.execute();
	}


	@Override
	public void excluirConvite(int position) {
		stopRemoveConviteTask();
		if(removerConviteTask == null ){
			removerConviteTask = new RemoverConviteTask(getActivity(), convites.get(position));
		}

		removerConviteTask.execute();
	}

	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(getActivity());
		}
		
		return provider;
	}
	
	private void executeGetConvitesTask() {
		stopConvitesTask();
		if(getAllConvitesTask == null ){
			getAllConvitesTask = new GetAllConvitesTask(getActivity());
		}

		getAllConvitesTask.execute();
	}

	private void stopConvitesTask() {
		if(getAllConvitesTask != null ){
			getAllConvitesTask.cancel(true);
		}

		getAllConvitesTask = null;
	}
	
	private void stopSendConviteTask() {
		if(sendConviteTask != null ){
			sendConviteTask.cancel(true);
		}

		sendConviteTask = null;
	}
	
	private ConviteCloud conviteCloud(){
		if(conviteCloud == null){
			conviteCloud = new ConviteCloud(getActivity());
		}
		
		return conviteCloud;
	}
	
	
	private void stopRemoveConviteTask() {
		if(removerConviteTask != null){
			removerConviteTask.cancel(true);
			
		}
		removerConviteTask = null;
	}
	
	class RemoverConviteTask extends AsyncTask<Void, Void,Void> {
 		ProgressDialog mDialog;
 		Context context;
 		
 		Convite convite;
 		int ok = 0;

 		RemoverConviteTask(Context context, Convite convite) {
 			this.context = context;
 			this.convite = convite;
 			mDialog = ProgressDialog.show(context, "", context.getString(R.string.removendo_convite), false, true);
 		}


		@Override
		protected Void doInBackground(Void... params) {
			removeOnCloud();
			return null;
		}
		
		private void removeOnCloud() {
			try {
				conviteCloud().removeConvite(convite);
			} catch (Exception e) {
				LogUtil.e("Erro ao remover convite", e);
			}
		}

		@Override
		protected void onPostExecute(Void result) {
			mDialog.dismiss();
			mDialog = null;
			if(conviteCloud().getResponseCode() != 204){
				AppHelper.getInstance().presentError(context, context.getString(R.string.erro_ao_remover_convite), context.getString(R.string.falha_de_conexao));
			}else{
				AppHelper.getInstance().presentError(context, context.getString(R.string.convite_removido_com_sucesso), context.getString(R.string.seu_convite_foi_removido_com_sucesso_do_servidor));
			}
			stopRemoveConviteTask();
			executeGetConvitesTask();
			super.onPostExecute(result);
		}


 	}

	class GetAllConvitesTask extends AsyncTask<String, Void, String> {
		ProgressDialog mDialog;
		Context context;

		ConviteCloud cloud;


		GetAllConvitesTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "", context.getString(R.string.buscando_os_convites), false, true);
			cloud = new ConviteCloud(context);
		}

		@Override
		public String doInBackground(String... params){
			return fetchAlarmes();
		}

		String fetchAlarmes(){
			try {
				cloud.getAllConvites();
				if (cloud.getResponseCode() != 200) {
					String erro = cloud.getErrorMessage();
					if(erro == null){
						erro = cloud.getResponseCode() + "";
					}
					return erro;
				}
			} catch (Throwable e) {
				return e.getLocalizedMessage();
			}
			return null;
		}

		@Override
		public void onPostExecute(String erroMessage) {
			mDialog.dismiss();
			if (erroMessage != null) {
				AppHelper.getInstance().presentError(context,getResources().getString(R.string.CONNECTION_ERROR),erroMessage);
			}
			populateConvites();

		}

	}


	class SendConviteTask extends AsyncTask<Void, Void,Void> {
 		ProgressDialog mDialog;
 		Context context;
 		
 		Convite convite;
 		String email;
 		int ok = 0;

 		SendConviteTask(Context context, Convite convite) {
 			this.context = context;
 			this.convite = convite;
 			mDialog = ProgressDialog.show(context, "", context.getString(R.string.sending_convite), false, true);
 		}


		@Override
		protected Void doInBackground(Void... params) {
			createConviteOnCloud();
			return null;
		}
		
		private void createConviteOnCloud() {
			try {
				conviteCloud().updateConvite(convite);
			} catch (Exception e) {
				LogUtil.e("Erro ao enviar convite", e);
			}
		}

		@Override
		protected void onPostExecute(Void result) {
			mDialog.dismiss();
			mDialog = null;
			if(conviteCloud().getResponseCode() != HttpStatus.SC_NO_CONTENT){
				AppHelper.getInstance().presentError(context, context.getString(R.string.erro_ao_enviar_convite), context.getString(R.string.falha_de_conexao));
			}else{
				AppHelper.getInstance().presentError(context, context.getString(R.string.convite_enviado_com_sucesso), context.getString(R.string.seu_convite_foi_enviado_com_sucesso));
			}
			stopSendConviteTask();
			executeGetConvitesTask();
			super.onPostExecute(result);
		}


 	}

}
