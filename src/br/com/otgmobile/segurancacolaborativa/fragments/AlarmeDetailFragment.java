package br.com.otgmobile.segurancacolaborativa.fragments;

import java.util.List;

import org.apache.http.HttpStatus;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.adapter.UsuarioAdapter;
import br.com.otgmobile.segurancacolaborativa.cloud.AlarmeCloud;
import br.com.otgmobile.segurancacolaborativa.cloud.AtendimentoCloud;
import br.com.otgmobile.segurancacolaborativa.database.DatabaseAccessProvider;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.model.Atendimento;
import br.com.otgmobile.segurancacolaborativa.model.AtendimentoStatus;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;
import br.com.otgmobile.segurancacolaborativa.util.AppHelper;
import br.com.otgmobile.segurancacolaborativa.util.ConstUtil;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;
import br.com.otgmobile.segurancacolaborativa.util.Session;

import com.actionbarsherlock.app.SherlockFragment;

public class AlarmeDetailFragment extends SherlockFragment implements OnRefreshListener {

	public final static String TAG ="alarmeDetailFragment";
	private Alarme alarme;
	private List<Usuario> usuarios;
	List<Atendimento> userAtendimentos;
	private TextView username;
	private TextView date;
	private TextView time;
	private TextView location;
	private TextView originName;
	private Button btnInsdisponivel;
	private Button btnAtender;
	private ListView userList;
	private AlarmeCloud alarmeCloud;
	private AtendimentoCloud atendimentoCloud;
	private DatabaseAccessProvider provider;
	private static GetupdatedAlarmeTask getupdatedAlarmeTask;
	private static CreateAtendimentoTask createAtendimentoTask;
	private SwipeRefreshLayout swipeLayout;



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		alarme = (Alarme) getActivity().getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
		View view = inflater.inflate(R.layout.alarme_detail, container, false);
		instatiateVisualComponents(view);
		swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.alarmDetail);
		swipeLayout.setOnRefreshListener(this);
		return view;
	}
	
	@Override public void onRefresh() {
        new Handler().post(new Runnable() {
            @Override public void run() {
            	executeUpdateAlarmeTask();
                swipeLayout.setRefreshing(false);
            }
        });
    }

	private void instatiateVisualComponents(View view) {
		username =(TextView) view.findViewById(R.id.username);
		date = (TextView) view.findViewById(R.id.date);
		time = (TextView) view.findViewById(R.id.time);
		location = (TextView) view.findViewById(R.id.location);
		originName = (TextView) view.findViewById(R.id.originName);
		btnAtender = (Button) view.findViewById(R.id.btnAtender);
		btnInsdisponivel = (Button) view.findViewById(R.id.btnInsdisponivel);
		userList = (ListView) view.findViewById(R.id.usersList);
		userList.setOnItemClickListener(listListener);
		btnAtender.setOnClickListener(atenderOnClickListener);
		btnInsdisponivel.setOnClickListener(indisponivelOnClickListener);
		checkAtendimentoStatus();
	}

	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
			stopUpdateAlarmeTask();
			executeUpdateAlarmeTask();			
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onDestroyView() {
		stopCreateAtendimentoTask();
		stopUpdateAlarmeTask();
		super.onDestroyView();
	}
	

	private void executeUpdateAlarmeTask() {
		if(getupdatedAlarmeTask == null){
			getupdatedAlarmeTask = new GetupdatedAlarmeTask(getActivity(), alarme);
		}
		getupdatedAlarmeTask.execute();

	}

	private void stopUpdateAlarmeTask() {
		if(getupdatedAlarmeTask != null){
			getupdatedAlarmeTask.cancel(!getupdatedAlarmeTask.isCancelled());
		}
		
		getupdatedAlarmeTask = null;

	}

	private void setAlarmeOnForm() {
		if(alarme == null) return;

		if(alarme.getData() != null){
			date.setText(AppHelper.getInstance().formateDate(getActivity(), alarme.getData()));
			time.setText(AppHelper.getInstance().formateTime(getActivity(), alarme.getData()));
		}

		if(alarme.getSensor() !=null && alarme.getSensor().getLocaizacao() != null){
			location.setText(alarme.getSensor().getLocaizacao());
		}

		if(alarme.getSensor() != null && alarme.getSensor().getTipoSensor() != null && alarme.getSensor().getTipoSensor().getNome()!= null){
			originName.setText(alarme.getSensor().getTipoSensor().getNome());
		}

		if(alarme.getSensor() != null && alarme.getSensor().getUsuario() != null && alarme.getSensor().getUsuario().getName() != null){
			username.setText(alarme.getSensor().getUsuario().getName());
			createUSuarioList(alarme.getSensor().getUsuario());
		}

	}

	private void createUSuarioList(Usuario usuario) {
		usuarios = provider().getUsuariosFromGrupoColaborativo(usuario);
		userList.setAdapter(new UsuarioAdapter(getActivity(), usuarios,alarme));
		setButtonLabel();
	}

	class GetupdatedAlarmeTask extends AsyncTask<Void, Void,Void> {
		ProgressDialog mDialog;
		Context context;

		Alarme alarme;
		int ok = 0;

		GetupdatedAlarmeTask(Context context, Alarme alarme) {
			this.context = context;
			this.alarme = alarme;
			mDialog = ProgressDialog.show(context, "", context.getString(R.string.atualizando_informa_es_do_alarme), false, true);
		}


		@Override
		protected Void doInBackground(Void... params) {
			createAlarmeOnCloud();
			return null;
		}

		private void createAlarmeOnCloud() {
			try {
				ok = alarmeCloud().getupadatedAlarme(alarme);
			} catch (Exception e) {
				LogUtil.e("Erro ao atualizar alarme", e);
				AppHelper.getInstance().presentError(context, context.getString(R.string.erro_ao_atualizar_as_informa_es_do_alarme), context.getString(R.string.falha_de_conexao)+e.getMessage());
			}
		}


		@Override
		protected void onPostExecute(Void result) {
			mDialog.dismiss();
			mDialog = null;
			if(ok != HttpStatus.SC_OK){
				AppHelper.getInstance().presentError(context, context.getString(R.string.erro_ao_atualizar_as_informa_es_do_alarme), context.getString(R.string.falha_de_conexao));
			}
			alarme = provider().getAlarmeFromDb(alarme.getId());
			setAlarmeOnForm();
			stopUpdateAlarmeTask();
			super.onPostExecute(result);
		}

	}

	private AlarmeCloud alarmeCloud(){
		if(alarmeCloud == null){
			alarmeCloud = new AlarmeCloud(getSherlockActivity());
		}

		return alarmeCloud;
	}

	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(getActivity());
		}

		return provider;
	}
	
	
	OnClickListener atenderOnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			intializeOrFinishAtendimento();
			
		}

		
	};
	
	OnClickListener indisponivelOnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Atendimento atendimento = new Atendimento();
			atendimento.setInicio(AppHelper.getCurrentDate());
			atendimento.setAlarmeId(alarme.getId());
			atendimento.setStatus(AtendimentoStatus.indisponivel.toString());
			createAtendimento(atendimento,true);
			
		}
	};
	
	private void setButtonLabel(){
		userAtendimentos = provider().findByEmail(Session.getEmailFromPreferences(getActivity()),alarme);
		if(userAtendimentos == null || userAtendimentos.isEmpty()){
			btnAtender.setText("Atender");
		}else if(isEmAtendimento()){
			btnAtender.setText("Finalizar");
		}else if(isFinalizado()){
			btnAtender.setText("Atendido");
			btnAtender.setBackground(getResources().getDrawable(R.drawable.indisponivel_button));
			btnAtender.setEnabled(false);
			btnInsdisponivel.setEnabled(false);
		}
	}
	
	
	private boolean isEmAtendimento() {
		for(Atendimento atendimento :userAtendimentos){
			if((atendimento.getStatus() != null && atendimento.getStatus().equals(AtendimentoStatus.atendendo.toString()))&&!isFinalizado()){
				return true;
			}
		}
		return false;
	}

	private boolean isFinalizado() {
		for(Atendimento atendimento :userAtendimentos){
			if(atendimento.getStatus() != null && atendimento.getStatus().equals(AtendimentoStatus.atendido.toString())){
				return true;
			}
		}
		return false;
	}

	private void intializeOrFinishAtendimento() {
		if(!checkAtendimentoStatus()){
			Atendimento atendimento = new Atendimento();
			atendimento.setAlarmeId(alarme.getId());
			atendimento.setInicio(AppHelper.getCurrentDate());
			atendimento.setStatus(AtendimentoStatus.atendendo.toString());
			createAtendimento(atendimento,true);			
		}else{
			for(Atendimento atendimento :userAtendimentos){
				if(atendimento.getStatus() != null && atendimento.getStatus().equals(AtendimentoStatus.atendendo.toString())){
					atendimento.setStatus(AtendimentoStatus.atendido.toString());
					atendimento.setFim(AppHelper.getCurrentDate());
					atendimento.setAlarme(null);
					atendimento.setAlarmeId(alarme.getId());
					createAtendimento(atendimento,false);
				}
			}
		}
	}
	
	private Boolean checkAtendimentoStatus() {
		Boolean a = false;
		userAtendimentos = provider().findByEmail(Session.getEmailFromPreferences(getActivity()),alarme);
		if(userAtendimentos == null || userAtendimentos.isEmpty()){
			return a;
		}
		for (Atendimento atendimento : userAtendimentos) {
			if(atendimento.getStatus()!= null && atendimento.getStatus().equals(AtendimentoStatus.atendendo.toString())){
				a = true;
			}
		}
		
		return a;
		
	}

	protected void createAtendimento(Atendimento atendimento, boolean create) {
		createAtendimentoTask(atendimento,create);
	}
	
	private void createAtendimentoTask(Atendimento atendimento, boolean create) {
		stopCreateAtendimentoTask();
		if(createAtendimentoTask== null){
			createAtendimentoTask = new CreateAtendimentoTask(getActivity(), atendimento, create);
		}
		
		createAtendimentoTask.execute();
		
	}

	class CreateAtendimentoTask extends AsyncTask<Void, Void,Void> {
		ProgressDialog mDialog;
		Context context;
		boolean create;

		Atendimento atendimento;
		int ok = 0;

		CreateAtendimentoTask(Context context, Atendimento atendimento, boolean create) {
			this.context = context;
			this.atendimento = atendimento;
			this.create = create;
			mDialog = ProgressDialog.show(context, "", context.getString(R.string.atualizando_estado_de_atendimento), false, true);
		}


		@Override
		protected Void doInBackground(Void... params) {
			createAtendimentoOnCloud();
			return null;
		}

		private void createAtendimentoOnCloud() {
			try {
				if(create){
					ok = atendimentoCloud().createAtendimento(atendimento,alarme);					
				}else{
					ok = atendimentoCloud().updateAtendimento(atendimento,alarme);										
				}
			} catch (Exception e) {
				LogUtil.e("Erro ao criar alarme", e);
				AppHelper.getInstance().presentError(context, context.getString(R.string.erro_ao_atualizar_o_estado_de_atendimento_para_o_alarme), context.getString(R.string.falha_de_conexao)+e.getMessage());
			}
		}


		@Override
		protected void onPostExecute(Void result) {
			mDialog.dismiss();
			mDialog = null;
			if(ok != HttpStatus.SC_CREATED){
				AppHelper.getInstance().presentError(context, context.getString(R.string.erro_ao_atualizar_o_estado_de_atendimento_para_o_alarme), context.getString(R.string.falha_de_conexao));
			}
			alarme = provider().getAlarmeFromDb(alarme.getId());
			setAlarmeOnForm();
			stopCreateAtendimentoTask();
			super.onPostExecute(result);
		}

	}
	
	private void stopCreateAtendimentoTask() {
		if(createAtendimentoTask !=null){
			createAtendimentoTask.cancel(!createAtendimentoTask.isCancelled());
		}
		createAtendimentoTask = null;
	}
	
	
	private AtendimentoCloud atendimentoCloud(){
		if(atendimentoCloud == null){
			atendimentoCloud = new AtendimentoCloud(getActivity());
		}
		
		return atendimentoCloud;
	}
	
	private OnItemClickListener listListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int position,long id) {
			Usuario usuario = usuarios.get(position);
			callUser(usuario);
			
		}
	}; 
	
	private void callUser(Usuario usuario) {
		if(usuario.getCelular() != null){
			call(usuario.getCelular());
		}
		
	}
	
	private void call(String number) {
	    try {
	        Intent callIntent = new Intent(Intent.ACTION_CALL);
	        callIntent.setData(Uri.parse(number));
	        startActivity(callIntent);
	    } catch (ActivityNotFoundException e) {
	        LogUtil.e("Erro ao efetutar chamada", e);
	    }
	}
	
}
