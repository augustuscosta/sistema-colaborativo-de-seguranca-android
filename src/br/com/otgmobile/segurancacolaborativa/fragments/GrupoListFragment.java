package br.com.otgmobile.segurancacolaborativa.fragments;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import org.apache.http.HttpStatus;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.activity.GroupDetailActivity;
import br.com.otgmobile.segurancacolaborativa.adapter.GrupoColaborativoAdapter;
import br.com.otgmobile.segurancacolaborativa.cloud.ConviteCloud;
import br.com.otgmobile.segurancacolaborativa.cloud.GrupoColaborativoCloud;
import br.com.otgmobile.segurancacolaborativa.database.DatabaseAccessProvider;
import br.com.otgmobile.segurancacolaborativa.listeners.GrupoConvidar;
import br.com.otgmobile.segurancacolaborativa.listeners.GrupoExcluir;
import br.com.otgmobile.segurancacolaborativa.listeners.GrupoPreview;
import br.com.otgmobile.segurancacolaborativa.model.Convite;
import br.com.otgmobile.segurancacolaborativa.model.GrupoColaborativo;
import br.com.otgmobile.segurancacolaborativa.util.AppHelper;
import br.com.otgmobile.segurancacolaborativa.util.ConstUtil;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;

import com.actionbarsherlock.app.SherlockListFragment;

public class GrupoListFragment extends SherlockListFragment implements GrupoExcluir, GrupoConvidar, GrupoPreview, OnRefreshListener{
	
	public final static String TAG ="gruposlistFragment";
	private List<GrupoColaborativo> grupos;
	GrupoColaborativoAdapter adapter;
	DatabaseAccessProvider provider;
	private static GetAllGruposTask getAllGruposTask;
	private SwipeRefreshLayout swipeLayout;
	private GrupoColaborativoCloud grupoCloud;
	private RemoverGrupoTask removerGrupoTask;
	private EditText conviteDialogInputText;
	private ConviteCloud conviteCloud;
	private SendConviteTask sendConviteTask;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_grupo_list, container, false);
		swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.grupoList);
		swipeLayout.setOnRefreshListener(this);
		return view;
	}
	
	
	@Override
	public void onResume() {
		executeGetGruposTask();
		super.onResume();
	}
	

	@Override public void onRefresh() {
        new Handler().post(new Runnable() {
            @Override public void run() {
            	executeGetGruposTask();
                swipeLayout.setRefreshing(false);
            }
        });
    }
	
	public void populateGrupos() {
		try {
			grupos = provider().grupoColaborativoDao().queryBuilder().orderBy("id", false).query();
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar os grupos", e);
			grupos = Collections.emptyList();
		}
		adapter = new GrupoColaborativoAdapter(getActivity(), grupos, this, this, this);
		setListAdapter(adapter);
	}
	
	
	@Override
	public void onDestroyView() {
		grupos = null;
		provider = null;
		super.onDestroyView();
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		viewGrupoDetails(position);
	}

	@Override
	public void convidarParaGrupo(final int position) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getString(R.string.enviar_convite));
		conviteDialogInputText = new EditText(getActivity());
		conviteDialogInputText.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS);
		conviteDialogInputText.setText("");
		builder.setView(conviteDialogInputText);

		// Set up the buttons
		builder.setPositiveButton(getString(R.string.send), new DialogInterface.OnClickListener() { 
			@Override
		    public void onClick(DialogInterface dialog, int which) {
		    	if(!isValid(conviteDialogInputText)){
		    		AppHelper.getInstance().presentError(getActivity(), getActivity().getString(R.string.erro_ao_enviar_convite), getActivity().getString(R.string.error_invalid_email));
		    		return;
		    	}
		    		
		        String email = conviteDialogInputText.getText().toString();
		        stopSendConviteGrupoTask();
		        SendConviteTask task = new SendConviteTask(getActivity(), grupos.get(position), email);
		        task.execute();
		    }
		});
		builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        dialog.cancel();
		    }
		});

		builder.show();
	}
	
	private Boolean isValid(EditText conviteDialogInputText){
		// Check for a valid email address.
        if (TextUtils.isEmpty(conviteDialogInputText.getText())) {
        	conviteDialogInputText.setError(getString(R.string.error_field_required));
        	conviteDialogInputText.requestFocus();
            return false;
        } else if (!conviteDialogInputText.getText().toString().contains("@")) {
        	conviteDialogInputText.setError(getString(R.string.error_invalid_email));
        	conviteDialogInputText.requestFocus();
            return false;
        }
        return true;
	}

	@Override
	public void viewGrupoDetails(int position) {
		Intent intent = new Intent(getActivity(),GroupDetailActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, grupos.get(position));
		bundle.putInt(ConstUtil.PAGE, 0);
		intent.putExtras(bundle);
		startActivity(intent);
	}
	
	
	@Override
	public void excluirGrupo(int position) {
		stopRemoveGrupoTask();
		if(removerGrupoTask == null ){
			removerGrupoTask = new RemoverGrupoTask(getActivity(), grupos.get(position));
		}

		removerGrupoTask.execute();
	}
	
	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(getActivity());
		}
		
		return provider;
	}
	
	private void executeGetGruposTask() {
		stopGruposTask();
		if(getAllGruposTask == null ){
			getAllGruposTask = new GetAllGruposTask(getActivity());
		}

		getAllGruposTask.execute();
	}

	private void stopGruposTask() {
		if(getAllGruposTask != null ){
			getAllGruposTask.cancel(true);
		}

		getAllGruposTask = null;
	}
	
	private void stopSendConviteGrupoTask() {
		if(sendConviteTask != null ){
			sendConviteTask.cancel(true);
		}

		sendConviteTask = null;
	}
	
	private GrupoColaborativoCloud grupoCloud(){
		if(grupoCloud == null){
			grupoCloud = new GrupoColaborativoCloud(getActivity());
		}
		
		return grupoCloud;
	}
	
	private ConviteCloud conviteCloud(){
		if(conviteCloud == null){
			conviteCloud = new ConviteCloud(getActivity());
		}
		
		return conviteCloud;
	}
	
	private void stopRemoveGrupoTask() {
		if(removerGrupoTask != null){
			removerGrupoTask.cancel(true);
			
		}
		removerGrupoTask = null;
	}
	
	class RemoverGrupoTask extends AsyncTask<Void, Void,Void> {
 		ProgressDialog mDialog;
 		Context context;
 		
 		GrupoColaborativo grupo;
 		int ok = 0;

 		RemoverGrupoTask(Context context, GrupoColaborativo grupo) {
 			this.context = context;
 			this.grupo = grupo;
 			mDialog = ProgressDialog.show(context, "", context.getString(R.string.removendo_grupo), false, true);
 		}


		@Override
		protected Void doInBackground(Void... params) {
			createAlarmeOnCloud();
			return null;
		}
		
		private void createAlarmeOnCloud() {
			try {
				grupoCloud().removeGrupo(grupo);
			} catch (Exception e) {
				LogUtil.e("Erro ao remover grupo", e);
			}
		}

		@Override
		protected void onPostExecute(Void result) {
			mDialog.dismiss();
			mDialog = null;
			if(grupoCloud().getResponseCode() != 204){
				AppHelper.getInstance().presentError(context, context.getString(R.string.erro_ao_remover_grupo), context.getString(R.string.falha_de_conexao));
			}else{
				AppHelper.getInstance().presentError(context, context.getString(R.string.grupo_removido_com_sucesso), context.getString(R.string.seu_grupo_foi_removido_com_sucesso_do_servidor));
			}
			stopRemoveGrupoTask();
			executeGetGruposTask();
			super.onPostExecute(result);
		}


 	}

	class GetAllGruposTask extends AsyncTask<String, Void, String> {
		ProgressDialog mDialog;
		Context context;

		GrupoColaborativoCloud cloud;


		GetAllGruposTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "", context.getString(R.string.buscando_os_grupos), false, true);
			cloud = new GrupoColaborativoCloud(context);
		}

		@Override
		public String doInBackground(String... params){
			return fetchAlarmes();
		}

		String fetchAlarmes(){
			try {
				cloud.getAllGrupos();
				if (cloud.getResponseCode() != 200) {
					String erro = cloud.getErrorMessage();
					if(erro == null){
						erro = cloud.getResponseCode() + "";
					}
					return erro;
				}
			} catch (Throwable e) {
				return e.getLocalizedMessage();
			}
			return null;
		}

		@Override
		public void onPostExecute(String erroMessage) {
			mDialog.dismiss();
			if (erroMessage != null) {
				AppHelper.getInstance().presentError(context,getResources().getString(R.string.CONNECTION_ERROR),erroMessage);
			}
			populateGrupos();

		}

	}


	class SendConviteTask extends AsyncTask<Void, Void,Void> {
 		ProgressDialog mDialog;
 		Context context;
 		
 		GrupoColaborativo grupo;
 		String email;
 		int ok = 0;

 		SendConviteTask(Context context, GrupoColaborativo grupo, String email) {
 			this.context = context;
 			this.grupo = grupo;
 			this.email = email;
 			mDialog = ProgressDialog.show(context, "", context.getString(R.string.sending_convite), false, true);
 		}


		@Override
		protected Void doInBackground(Void... params) {
			createConviteOnCloud();
			return null;
		}
		
		private void createConviteOnCloud() {
			try {
				Convite convite = new Convite();
				convite.setEmailDestinatario(email);
				convite.setGrupoId(grupo.getId());
				convite.setGrupoNome(grupo.getNome());
				conviteCloud().createConvite(convite);
			} catch (Exception e) {
				LogUtil.e("Erro ao enviar convite", e);
			}
		}

		@Override
		protected void onPostExecute(Void result) {
			mDialog.dismiss();
			mDialog = null;
			if(conviteCloud().getResponseCode() != HttpStatus.SC_CREATED){
				AppHelper.getInstance().presentError(context, context.getString(R.string.erro_ao_enviar_convite), context.getString(R.string.falha_de_conexao));
			}else{
				AppHelper.getInstance().presentError(context, context.getString(R.string.convite_enviado_com_sucesso), context.getString(R.string.seu_convite_foi_enviado_com_sucesso));
			}
			stopSendConviteGrupoTask();
			super.onPostExecute(result);
		}


 	}
}
