package br.com.otgmobile.segurancacolaborativa.fragments;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.adapter.UsuarioGrupoAdapter;
import br.com.otgmobile.segurancacolaborativa.cloud.GrupoColaborativoCloud;
import br.com.otgmobile.segurancacolaborativa.database.DatabaseAccessProvider;
import br.com.otgmobile.segurancacolaborativa.listeners.UsuarioExcluir;
import br.com.otgmobile.segurancacolaborativa.model.GrupoColaborativo;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;
import br.com.otgmobile.segurancacolaborativa.util.AppHelper;
import br.com.otgmobile.segurancacolaborativa.util.ConstUtil;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;
import br.com.otgmobile.segurancacolaborativa.util.Session;

import com.actionbarsherlock.app.SherlockListFragment;

public class UsuarioGrupoListFragment extends SherlockListFragment implements UsuarioExcluir, OnRefreshListener{
	
	public final static String TAG ="usuarioGrupolistFragment";
	private GrupoColaborativo grupo;
	UsuarioGrupoAdapter adapter;
	DatabaseAccessProvider provider;
	private static GetAllGruposTask getAllGruposTask;
	private SwipeRefreshLayout swipeLayout;
	private RemoverGrupoTask removerGrupoTask;
	private GrupoColaborativoCloud grupoCloud;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_usuario_grupo_list, container, false);
		swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.usuarioList);
		swipeLayout.setOnRefreshListener(this);
		return view;
	}
	
	@Override
	public void onResume() {
		grupo = (GrupoColaborativo) getActivity().getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
		executeGetGruposTask();
		super.onResume();
	}
	
	@Override public void onRefresh() {
        new Handler().post(new Runnable() {
            @Override public void run() {
            	executeGetGruposTask();
                swipeLayout.setRefreshing(false);
            }
        });
    }
	
	public void populateGrupos() {
		try {
			List<GrupoColaborativo> result = provider().grupoColaborativoDao().queryBuilder().where().eq("id", grupo.getId()).query();
			if(result != null && result.size() > 0){
				grupo = result.get(0);
				adapter = new UsuarioGrupoAdapter(getActivity(), getUsuarios(), grupo, Session.getEmailFromPreferences(getActivity()), this);
				setListAdapter(adapter);
			}else{
				adapter = new UsuarioGrupoAdapter(getActivity(), new ArrayList<Usuario>(), grupo, Session.getEmailFromPreferences(getActivity()), this);
				setListAdapter(adapter);
			}
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar os grupos", e);
		}
	}
	
	
	@Override
	public void onDestroyView() {
		provider = null;
		super.onDestroyView();
	}
	

	@Override
	public void excluir(int position) {
		stopRemoveGrupoTask();
		if(removerGrupoTask == null ){
			removerGrupoTask = new RemoverGrupoTask(getActivity(), grupo, getUsuarios().get(position));
		}

		removerGrupoTask.execute();
	}
	
	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(getActivity());
		}
		
		return provider;
	}
	
	private void executeGetGruposTask() {
		stopGruposTask();
		if(getAllGruposTask == null ){
			getAllGruposTask = new GetAllGruposTask(getActivity());
		}

		getAllGruposTask.execute();
	}

	private void stopGruposTask() {
		if(getAllGruposTask != null ){
			getAllGruposTask.cancel(true);
		}

		getAllGruposTask = null;
	}
	
	
	private GrupoColaborativoCloud grupoCloud(){
		if(grupoCloud == null){
			grupoCloud = new GrupoColaborativoCloud(getActivity());
		}
		
		return grupoCloud;
	}
	
	
	private void stopRemoveGrupoTask() {
		if(removerGrupoTask != null){
			removerGrupoTask.cancel(true);
			
		}
		removerGrupoTask = null;
	}
	
	private List<Usuario> getUsuarios(){
		if(grupo!=null)
			return provider().getUsuariosFromGrupoColaborativo(grupo);
		
		return Collections.emptyList();
	}
	
	class RemoverGrupoTask extends AsyncTask<Void, Void,Void> {
 		ProgressDialog mDialog;
 		Context context;
 		Usuario usuario;
 		GrupoColaborativo grupo;
 		int ok = 0;

 		RemoverGrupoTask(Context context, GrupoColaborativo grupo, Usuario usuario) {
 			this.context = context;
 			this.grupo = grupo;
 			this.usuario = usuario;
 			mDialog = ProgressDialog.show(context, "", context.getString(R.string.removendo_usuario_grupo), false, true);
 		}


		@Override
		protected Void doInBackground(Void... params) {
			removeUsuarioGromGrupo();
			return null;
		}
		
		private void removeUsuarioGromGrupo() {
			try {
				grupoCloud().removerUsuarioDoGrupo(grupo, usuario);
			} catch (Exception e) {
				LogUtil.e("Erro ao remover usuário do grupo grupo", e);
			}
		}

		@Override
		protected void onPostExecute(Void result) {
			mDialog.dismiss();
			mDialog = null;
			if(grupoCloud().getResponseCode() != 204){
				AppHelper.getInstance().presentError(context, context.getString(R.string.erro_ao_remover_usuario_grupo), context.getString(R.string.falha_de_conexao));
			}else{
				AppHelper.getInstance().presentError(context, context.getString(R.string.usuario_grupo_removido_com_sucesso), context.getString(R.string.usuario_grupo_foi_removido_com_sucesso_do_servidor));
			}
			stopRemoveGrupoTask();
			executeGetGruposTask();
			super.onPostExecute(result);
		}


 	}

	class GetAllGruposTask extends AsyncTask<String, Void, String> {
		ProgressDialog mDialog;
		Context context;

		GrupoColaborativoCloud cloud;


		GetAllGruposTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "", context.getString(R.string.buscando_os_usuarios), false, true);
			cloud = new GrupoColaborativoCloud(context);
		}

		@Override
		public String doInBackground(String... params){
			return fetchAlarmes();
		}

		String fetchAlarmes(){
			try {
				cloud.getAllGrupos();
				if (cloud.getResponseCode() != 200) {
					String erro = cloud.getErrorMessage();
					if(erro == null){
						erro = cloud.getResponseCode() + "";
					}
					return erro;
				}
			} catch (Throwable e) {
				return e.getLocalizedMessage();
			}
			return null;
		}

		@Override
		public void onPostExecute(String erroMessage) {
			mDialog.dismiss();
			if (erroMessage != null) {
				AppHelper.getInstance().presentError(context,getResources().getString(R.string.CONNECTION_ERROR),erroMessage);
			}
			populateGrupos();

		}

	}

}
