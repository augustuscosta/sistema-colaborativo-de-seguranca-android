package br.com.otgmobile.segurancacolaborativa.fragments;

import java.util.List;

import org.apache.http.HttpStatus;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.adapter.ComentarioAdapter;
import br.com.otgmobile.segurancacolaborativa.cloud.ComentarioCloud;
import br.com.otgmobile.segurancacolaborativa.database.DatabaseAccessProvider;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.model.Comentario;
import br.com.otgmobile.segurancacolaborativa.util.AppHelper;
import br.com.otgmobile.segurancacolaborativa.util.ConstUtil;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;

import com.actionbarsherlock.app.SherlockListFragment;

public class ComentarioListFragment extends SherlockListFragment implements OnRefreshListener{
	
	private List<Comentario> comentarios;
	private DatabaseAccessProvider provider;
	private Alarme alarme;
	private TextView comentarioText;
	private ComentarioCloud comentarioCloud;
	private Button sendComentButton;
	private CreateComentarioTask createComentarioTask;
	private SwipeRefreshLayout swipeLayout;
	
	public final static String TAG ="atendimentosFragment";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.atendimentos_list,container,false);
		comentarioText = (TextView) view.findViewById(R.id.comentarioText);
		sendComentButton = (Button) view.findViewById(R.id.sendComentButton);
		sendComentButton.setOnClickListener(sendButtonOnClickListener);
		swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.atendimentoList);
		swipeLayout.setOnRefreshListener(this);
		return view;
	}
	
	@Override public void onRefresh() {
        new Handler().post(new Runnable() {
            @Override public void run() {
            	refresh();
                swipeLayout.setRefreshing(false);
            }
        });
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		alarme = (Alarme) getActivity().getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
		refresh();
	}

	private void refresh() {
		comentarios = provider().getComentariosFromAlarme(alarme.getId());
		setListAdapter(new ComentarioAdapter(comentarios, getActivity()));
	}
	
	private OnClickListener sendButtonOnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
		 sendComentario();
		}

	};

	private void sendComentario() {
		if(comentarioText.getText().length() > 1){
			Comentario  comentario = new Comentario();
			comentario.setMensagem(comentarioText.getText().toString());
			comentario.setDate(AppHelper.getCurrentDate());	
			comentario.setAlarmId(alarme.getId());
			executeCreateComentatioTask(comentario);
		}
		
	}
	
	private void executeCreateComentatioTask(Comentario comentario) {
		if(createComentarioTask == null){
			createComentarioTask = new CreateComentarioTask(getActivity(), comentario);
		}
		
		createComentarioTask.execute();
	}
	private void stopCreateComentatioTask() {
		if(createComentarioTask != null){
			createComentarioTask.cancel(!createComentarioTask.isCancelled());
		}
		
		createComentarioTask = null;
	}

	class CreateComentarioTask extends AsyncTask<Void, Void,Void> {
 		ProgressDialog mDialog;
 		Context context;
 		
 		Comentario comentario;
 		int ok = 0;

 		CreateComentarioTask(Context context, Comentario comentario) {
 			this.context = context;
 			this.comentario = comentario;
 			mDialog = ProgressDialog.show(context, "", context.getString(R.string.enviando_coment_rio), false, true);
 		}


		@Override
		protected Void doInBackground(Void... params) {
			createComentarioOnCloud();
			return null;
		}
		
		private void createComentarioOnCloud() {
			try {
				ok = comentarioCloud().createComentario(comentario,alarme);
			} catch (Exception e) {
				LogUtil.e("Erro ao criar alarme", e);
				AppHelper.getInstance().presentError(context,context.getString(R.string.erro_ao_enviar_comentario), context.getString(R.string.falha_de_conexao)+e.getMessage());
			}
			
			
			
		}

		@Override
		protected void onPostExecute(Void result) {
			mDialog.dismiss();
			mDialog = null;
			if(ok != HttpStatus.SC_CREATED){
				AppHelper.getInstance().presentError(context, context.getString(R.string.erro_ao_enviar_comentario), context.getString(R.string.falha_de_conexao));
			}
			stopCreateComentatioTask();
			comentarioText.setText("");
			refresh();
			super.onPostExecute(result);
		}

 	}
	
	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(getActivity());
		}
		
		return provider;
	}

	public ComentarioCloud comentarioCloud() {
		if(comentarioCloud == null){
			comentarioCloud = new ComentarioCloud(getActivity());
		}
		
		return comentarioCloud;
	}
	

	
}
