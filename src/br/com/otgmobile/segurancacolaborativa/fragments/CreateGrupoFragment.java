package br.com.otgmobile.segurancacolaborativa.fragments;

import org.apache.http.HttpStatus;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.cloud.GrupoColaborativoCloud;
import br.com.otgmobile.segurancacolaborativa.model.GrupoColaborativo;
import br.com.otgmobile.segurancacolaborativa.util.AppHelper;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;
import br.com.otgmobile.segurancacolaborativa.util.Session;

import com.actionbarsherlock.app.SherlockFragment;

public class CreateGrupoFragment extends SherlockFragment{
	
	
	public final static String TAG ="createGrupoFragment";
	private TextView nomeTextView;
	private TextView descricaoTextView;
	private Button createGrupoButton;
	private GrupoColaborativoCloud grupoCloud;
	private CreateGrupoTask createGrupoTask;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_create_grupo, container, false);
		createGrupoButton = (Button) view.findViewById(R.id.create_group_button);
		nomeTextView = (TextView) view.findViewById(R.id.name);
		descricaoTextView = (TextView) view.findViewById(R.id.descricao);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		createGrupoButton.setOnClickListener(addListener);
	}
	
	
	
	OnClickListener addListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if(isValid()){
				GrupoColaborativo grupo = new GrupoColaborativo();
				grupo.setEmailDono(Session.getEmailFromPreferences(getActivity()));
				grupo.setNome(nomeTextView.getText().toString());
				grupo.setDescricao(descricaoTextView.getText().toString());
				stopCreateGrupoTask();
				createGrupoTask = new CreateGrupoTask(getActivity(), grupo);
				createGrupoTask.execute();
			}
		}
	};
	
	private Boolean isValid(){
		if (TextUtils.isEmpty(nomeTextView.getText().toString())) {
			nomeTextView.setError(getString(R.string.error_field_required));
			nomeTextView.requestFocus();
            return false;
        }
		if (TextUtils.isEmpty(descricaoTextView.getText().toString())) {
			descricaoTextView.setError(getString(R.string.error_field_required));
			descricaoTextView.requestFocus();
            return false;
        }
		return true;
	}
	

	@Override
	public void onDestroyView() {
		removeListeners();	
		stopCreateGrupoTask();
		super.onDestroyView();
	}

	private void removeListeners() {
		createGrupoButton.setOnClickListener(null);
	}
	
	private GrupoColaborativoCloud grupoCloud(){
		if(grupoCloud == null){
			grupoCloud = new GrupoColaborativoCloud(getActivity());
		}
		
		return grupoCloud;
	}
	
	private void stopCreateGrupoTask() {
		if(createGrupoTask != null){
			createGrupoTask.cancel(true);
			
		}
		createGrupoTask = null;
	}
	
	class CreateGrupoTask extends AsyncTask<Void, Void,Void> {
 		ProgressDialog mDialog;
 		Context context;
 		
 		GrupoColaborativo grupo;
 		int ok = 0;

 		CreateGrupoTask(Context context, GrupoColaborativo grupo) {
 			this.context = context;
 			this.grupo = grupo;
 			mDialog = ProgressDialog.show(context, "", context.getString(R.string.criando_grupo), false, true);
 		}


		@Override
		protected Void doInBackground(Void... params) {
			createAlarmeOnCloud();
			return null;
		}
		
		private void createAlarmeOnCloud() {
			try {
				ok = grupoCloud().createGrupo(grupo);
			} catch (Exception e) {
				LogUtil.e("Erro ao criar grupo", e);
			}
		}

		@Override
		protected void onPostExecute(Void result) {
			mDialog.dismiss();
			mDialog = null;
			if(ok != HttpStatus.SC_CREATED){
				AppHelper.getInstance().presentError(context, context.getString(R.string.erro_ao_criar_grupo), context.getString(R.string.falha_de_conexao));
			}else{
				AppHelper.getInstance().presentError(context, context.getString(R.string.grupo_criado_com_sucesso), context.getString(R.string.seu_grupo_foi_enviado_com_sucesso_ao_servidor));
				nomeTextView.setText("");
				descricaoTextView.setText("");
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
	    			      Context.INPUT_METHOD_SERVICE);
	    		imm.hideSoftInputFromWindow(descricaoTextView.getWindowToken(), 0);
			}
			stopCreateGrupoTask();
			super.onPostExecute(result);
		}


 	}
	
	
	
}
