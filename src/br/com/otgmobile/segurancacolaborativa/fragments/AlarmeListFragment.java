
package br.com.otgmobile.segurancacolaborativa.fragments;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.activity.AlarmeActivity;
import br.com.otgmobile.segurancacolaborativa.adapter.AlarmeAdapter;
import br.com.otgmobile.segurancacolaborativa.cloud.AlarmeCloud;
import br.com.otgmobile.segurancacolaborativa.database.DatabaseAccessProvider;
import br.com.otgmobile.segurancacolaborativa.listeners.AlarmeComentater;
import br.com.otgmobile.segurancacolaborativa.listeners.AlarmeLocater;
import br.com.otgmobile.segurancacolaborativa.listeners.AlarmePreview;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.util.AppHelper;
import br.com.otgmobile.segurancacolaborativa.util.ConstUtil;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;

import com.actionbarsherlock.app.SherlockListFragment;

public class AlarmeListFragment extends SherlockListFragment implements AlarmePreview, AlarmeLocater,AlarmeComentater, OnRefreshListener {
	
	public final static String TAG ="alarmelistFragment";
	private List<Alarme> alarmes;
	AlarmeAdapter adapter;
	DatabaseAccessProvider provider;
	private static GetAllAlarmsTask getAllAlarmsTask;
	private SwipeRefreshLayout swipeLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_alarme_list, container, false);
		swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.alarmList);
		swipeLayout.setOnRefreshListener(this);
		return view;
	}
	
	
	@Override
	public void onResume() {
		executeGetAlarmesTask();
		super.onResume();
	}
	
	@Override public void onRefresh() {
        new Handler().post(new Runnable() {
            @Override public void run() {
            	executeGetAlarmesTask();
                swipeLayout.setRefreshing(false);
            }
        });
    }

	public void populateAlarmes() {
		try {
			alarmes = provider().alarmeDao().queryBuilder().orderBy("id", false).query();
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar os alarmes", e);
			alarmes = Collections.emptyList();
		}
		adapter = new AlarmeAdapter(getActivity(), alarmes,this,this,this);
		setListAdapter(adapter);
	}
	
	
	@Override
	public void onDestroyView() {
		alarmes = null;
		provider = null;
		super.onDestroyView();
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		viewAlarmDetails(position);
	}

	@Override
	public void locateAlarme(int position) {
		Intent intent = new Intent(getActivity(),AlarmeActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, alarmes.get(position));
		bundle.putInt(ConstUtil.PAGE, 1);
		intent.putExtras(bundle);
		startActivity(intent);
	}

	@Override
	public void viewAlarmDetails(int position) {
		Intent intent = new Intent(getActivity(),AlarmeActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, alarmes.get(position));
		bundle.putInt(ConstUtil.PAGE, 0);
		intent.putExtras(bundle);
		startActivity(intent);
	}
	
	
	@Override
	public void comentAlarm(int position) {
		Intent intent = new Intent(getActivity(),AlarmeActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, alarmes.get(position));
		bundle.putInt(ConstUtil.PAGE, 2);
		intent.putExtras(bundle);
		startActivity(intent);
	}
	
	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(getActivity());
		}
		
		return provider;
	}
	
	private void executeGetAlarmesTask() {
		stopAlarmesTask();
		if(getAllAlarmsTask == null ){
			getAllAlarmsTask = new GetAllAlarmsTask(getActivity());
		}

		getAllAlarmsTask.execute();
	}

	private void stopAlarmesTask() {
		if(getAllAlarmsTask != null ){
			getAllAlarmsTask.cancel(true);
		}

		getAllAlarmsTask = null;
	}

	class GetAllAlarmsTask extends AsyncTask<String, Void, String> {
		ProgressDialog mDialog;
		Context context;

		AlarmeCloud cloud;


		GetAllAlarmsTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "", context.getString(R.string.buscando_os_ultimos_alarmes), false, true);
			cloud = new AlarmeCloud(context);
		}

		@Override
		public String doInBackground(String... params){
			return fetchAlarmes();
		}

		String fetchAlarmes(){
			try {
				cloud.getAllAlarmes();
				if (cloud.getResponseCode() != 200) {
					String erro = cloud.getErrorMessage();
					if(erro == null){
						erro = cloud.getResponseCode() + "";
					}
					return erro;
				}
			} catch (Throwable e) {
				return e.getLocalizedMessage();
			}
			return null;
		}

		@Override
		public void onPostExecute(String erroMessage) {
			mDialog.dismiss();
			if (erroMessage != null) {
				AppHelper.getInstance().presentError(context,getResources().getString(R.string.CONNECTION_ERROR),erroMessage);
			}
			populateAlarmes();

		}

	}

}
