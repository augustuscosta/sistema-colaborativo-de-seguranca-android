package br.com.otgmobile.segurancacolaborativa.fragments;

import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.util.AppHelper;
import br.com.otgmobile.segurancacolaborativa.util.ConstUtil;

import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class AlarmeMapFragment extends SherlockFragment {

	public final static String TAG ="alarmeMapFragment";
	private GoogleMap map;
	private Alarme alarme;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.alarme_map, null);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		initializeMap();
		alarme = (Alarme) getActivity().getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
		
		if(map!=null){
			addMarker();			
		}
		super.onActivityCreated(savedInstanceState);
	}
	
	private void initializeMap() {
		SupportMapFragment supportMapFragment = (SupportMapFragment)getActivity().getSupportFragmentManager().findFragmentById(R.id.map);
		map = supportMapFragment.getMap();
		if(map!= null){
			map.setMyLocationEnabled(true);
		}
	}

	private void addMarker() {
		if(getMarkerForAlarme()!= null){
			map.addMarker(getMarkerForAlarme());
		}else{
			AppHelper.getInstance().presentError(getActivity(), "Sem Localização", getSherlockActivity().getString(
					R.string.a_localizacao_da_emergencia_nao_foi_captada));
		}

	}

	private MarkerOptions getMarkerForAlarme(){
		String titulo = "";
		String descricao = "";
		LatLng position = null;
		if(alarme!= null && (alarme.getLatitude() != null && alarme.getLongitude()!=null)){
			position = new LatLng(alarme.getLatitude(), alarme.getLongitude());			
		}
		if(alarme.getSensor()!= null && alarme.getSensor().getTipoSensor()!= null && alarme.getSensor().getTipoSensor().getNome()!= null){
			titulo = alarme.getSensor().getTipoSensor().getNome();
		}

		if(alarme.getSensor() != null && alarme.getSensor().getUsuario() != null && alarme.getSensor().getUsuario().getName() != null){
			descricao =(alarme.getSensor().getUsuario().getName());
		}


		if(position!= null){
			MarkerOptions marker = new MarkerOptions()
			.position(position)
			.title(titulo)
			.snippet(descricao)
			.icon(BitmapDescriptorFactory.fromResource(R.drawable.button_locate));
			centerMapOnPosition(position);
			return marker;		
		}else{
			centerMapOnMyLocation();
			return null;
		}
	}

	private void centerMapOnPosition(LatLng position) {
		CameraUpdate center=CameraUpdateFactory.newLatLng(position);
		CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);
		map.moveCamera(center);
		map.animateCamera(zoom);
	}

	private void centerMapOnMyLocation() {
		Location location = map.getMyLocation();
		if(location!=null){
			LatLng locationCoordinates = new LatLng(location.getLatitude(), location.getLongitude());
			centerMapOnPosition(locationCoordinates);			
		}
	}

}
