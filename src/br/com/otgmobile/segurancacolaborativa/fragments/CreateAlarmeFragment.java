package br.com.otgmobile.segurancacolaborativa.fragments;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;

import com.actionbarsherlock.app.SherlockFragment;

public class CreateAlarmeFragment extends SherlockFragment{
	
	
	public final static String TAG ="createAlarmeFragment";
	private Button alarmeButton;
	private Button policeButton;
	private Button addButton;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_create_alarme, container, false);
		alarmeButton = (Button) view.findViewById(R.id.button_emergencia);
		policeButton = (Button) view.findViewById(R.id.button_police);
		addButton =(Button) view.findViewById(R.id.button_add);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		policeButton.setOnClickListener(policeListener);
		addButton.setOnClickListener(addListener);
	}
	
	
	
	OnClickListener addListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			call("tel:192");			
		}
	};
	
	OnClickListener policeListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			call("tel:190");			
		}
	};
	

	@Override
	public void onDestroyView() {
		removeListeners();	
		super.onDestroyView();
	}

	private void removeListeners() {
		alarmeButton.setOnClickListener(null);
		policeButton.setOnClickListener(null);
		addButton.setOnClickListener(null);
	}
	
	
	
	
	private void call(String number) {
	    try {
	        Intent callIntent = new Intent(Intent.ACTION_CALL);
	        callIntent.setData(Uri.parse(number));
	        startActivity(callIntent);
	    } catch (ActivityNotFoundException e) {
	        LogUtil.e("Erro ao efetutar chamada", e);
	    }
	}


}
