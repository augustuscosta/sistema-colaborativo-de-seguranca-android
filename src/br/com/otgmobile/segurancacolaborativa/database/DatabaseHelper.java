package br.com.otgmobile.segurancacolaborativa.database;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.model.Atendimento;
import br.com.otgmobile.segurancacolaborativa.model.Comentario;
import br.com.otgmobile.segurancacolaborativa.model.Convite;
import br.com.otgmobile.segurancacolaborativa.model.GrupoColaborativo;
import br.com.otgmobile.segurancacolaborativa.model.GrupoUsuario;
import br.com.otgmobile.segurancacolaborativa.model.Sensor;
import br.com.otgmobile.segurancacolaborativa.model.TipoSensor;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * @author brunoramosdias
 *
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

	private static final String DATABASE_NAME = "sistemacolaborativoseg.db";
	private static final int DATABASE_VERSION = 1;
	private static DatabaseHelper databaseHelper;

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
	}

	@Override
	public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
		try {
			TableUtils.createTable(arg1, TipoSensor.class);
			TableUtils.createTable(arg1, Sensor.class);
			TableUtils.createTable(arg1, Usuario.class);
			TableUtils.createTable(arg1, GrupoColaborativo.class);
			TableUtils.createTable(arg1, Alarme.class);
			TableUtils.createTable(arg1, Atendimento.class);
			TableUtils.createTable(arg1, Comentario.class);
			TableUtils.createTable(arg1, GrupoUsuario.class);
			TableUtils.createTable(arg1, Convite.class);
		} catch (SQLException e) {
			LogUtil.e("erro ao criar as tabelas do banco", e);
		}
		DaoManager.clearDaoCache();
	}

	public static OrmLiteSqliteOpenHelper getDatabase(Context context){
		if(databaseHelper == null){
			databaseHelper = new DatabaseHelper(context);
		}

		return databaseHelper;
	}


	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2,
			int arg3) {
	}

}
