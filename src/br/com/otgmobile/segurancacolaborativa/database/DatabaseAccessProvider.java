package br.com.otgmobile.segurancacolaborativa.database;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.model.Atendimento;
import br.com.otgmobile.segurancacolaborativa.model.Comentario;
import br.com.otgmobile.segurancacolaborativa.model.Convite;
import br.com.otgmobile.segurancacolaborativa.model.GrupoColaborativo;
import br.com.otgmobile.segurancacolaborativa.model.GrupoUsuario;
import br.com.otgmobile.segurancacolaborativa.model.Sensor;
import br.com.otgmobile.segurancacolaborativa.model.TipoSensor;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.support.ConnectionSource;


public class DatabaseAccessProvider {
	private DatabaseHelper databaseHelper;
	private Dao<Alarme, Long> alarmeDao;
	private Dao<TipoSensor, Long> tipoSensorDao;
	private Dao<Sensor, Long> sensorDao;
	private Dao<Usuario, Long> usuarioDao;
	private Dao<Atendimento, Long> atendimentoDao;
	private Dao<GrupoColaborativo, Long> grupoColaborativoDao;
	private Dao<GrupoUsuario, Long> grupoUsuarioDao;
	private Dao<Comentario, Long> comentarioDao;
	private Dao<Convite, Long> conviteDao;
	private PreparedQuery<Usuario> usuarioPreparedQuery;
	//	
	public DatabaseAccessProvider(Context context){
		databaseHelper = (DatabaseHelper) DatabaseHelper.getDatabase(context);
	}
	
	public  ConnectionSource getConnectionDataSource(){
		return databaseHelper.getConnectionSource();
	}


	//	//DAO Lazy Instantiantions
	public Dao<Alarme, Long> alarmeDao(){
		if(alarmeDao == null){
			try {
				alarmeDao = databaseHelper.getDao(Alarme.class);
			} catch (Exception e) {
				LogUtil.e("erro ao instanciar o dao", e);
			}
		}

		return alarmeDao;

	}
	
	public Dao<Convite, Long> conviteDao(){
		if(conviteDao == null){
			try {
				conviteDao = databaseHelper.getDao(Convite.class);
			} catch (Exception e) {
				LogUtil.e("erro ao instanciar o dao", e);
			}
		}

		return conviteDao;

	}


	public Dao<TipoSensor, Long> tipoSensorDao(){ 
		if(tipoSensorDao == null){
			try {
				tipoSensorDao= databaseHelper.getDao(TipoSensor.class);
			} catch (Exception e) {
				LogUtil.e("erro ao instanciar o dao", e);
			}
		}

		return tipoSensorDao;
	}

	public Dao<Sensor, Long> sensorDao(){
		if(sensorDao == null){
			try {
				sensorDao = databaseHelper.getDao(Sensor.class);
			} catch (Exception e) {
				LogUtil.e("erro ao instanciar o dao",e);
			}
		}

		return sensorDao;
	}

	public Dao<Usuario, Long> usuarioDao(){
		if(usuarioDao == null){
			try {
				usuarioDao = databaseHelper.getDao(Usuario.class);
			} catch (Exception e) {
				LogUtil.e("erro ao instanciar o dao",e);
			}
		}

		return usuarioDao;
	}

	public Dao<GrupoColaborativo, Long> grupoColaborativoDao(){
		if(grupoColaborativoDao == null){
			try {
				grupoColaborativoDao = databaseHelper.getDao(GrupoColaborativo.class);
			} catch (Exception e) {
				LogUtil.e("erro ao instanciar o dao",e);
			}
		}

		return grupoColaborativoDao;
	}

	public Dao<GrupoUsuario, Long> grupoUsuarioDao(){
		if(grupoUsuarioDao == null){
			try {
				grupoUsuarioDao = databaseHelper.getDao(GrupoUsuario.class);
			} catch (Exception e) {
				LogUtil.e("erro ao instanciar o dao",e);
			}
		}

		return grupoUsuarioDao;
	}

	public Dao<Comentario, Long> comentarioDao(){
		if(comentarioDao == null){
			try {
				comentarioDao = databaseHelper.getDao(Comentario.class);
			} catch (Exception e) {
				LogUtil.e("erro ao instanciar o dao",e);
			}
		}

		return comentarioDao;
	}

	public CreateOrUpdateStatus persitUsuarioWithGrupo(Usuario usuario){
		CreateOrUpdateStatus status = null;
		try {
			status = usuarioDao().createOrUpdate(usuario);
			if(usuario.getGruposColaborativos() != null){
				for(GrupoColaborativo grupo: usuario.getGruposColaborativos()){
					grupoColaborativoDao().createOrUpdate(grupo);
					for(Usuario usuario2 : grupo.getUsuarios()){
						usuarioDao().createOrUpdate(usuario2);
						persistGrupoUSuario(usuario2, grupo);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return status;
	}


	public void persistGrupoUSuario(Usuario usuario,GrupoColaborativo grupo)
			throws SQLException {
		List<GrupoUsuario> gruposUsuarios = grupoUsuarioDao().queryBuilder().where().eq(GrupoUsuario.GRUPO_ID_FIELD, grupo.getId()).and().eq(GrupoUsuario.USUARIO_ID_FIELD, usuario.getId()).query();
		GrupoUsuario fromDb = (gruposUsuarios == null || gruposUsuarios.isEmpty()) ? null :gruposUsuarios.get(0);
		if(fromDb == null){
			GrupoUsuario grupoUsuario = new GrupoUsuario();
			grupoUsuario.setGrupoColaborativo(grupo);
			grupoUsuario.setUsuario(usuario);
			grupoUsuarioDao().createOrUpdate(grupoUsuario);
		}
	}

	public Dao<Atendimento, Long> atendimentoDao(){
		if(atendimentoDao == null){
			try {
				atendimentoDao = databaseHelper.getDao(Atendimento.class);
			} catch (Exception e) {
				LogUtil.e("erro ao instanciar o dao",e);
			}
		}

		return atendimentoDao;
	}


	public Collection<Alarme> getAlarmesFromSensor(Long id){
		try {
			return alarmeDao().queryForEq("sensor", id);
		} catch (Exception e) {
			LogUtil.e("erro ao pergar alarmes do sensor", e);
			return null;
		}finally{
			try {
				alarmeDao().closeLastIterator();
			} catch (Exception e) {
				LogUtil.e("erro ao fechar cursor do objeto acesso", e);
			}
		}

	}

	public List<Atendimento> getAtendimentosFromAlarme(Long id){
		try {
			
			return atendimentoDao().queryBuilder().orderBy("id", false).where().eq("alarme", id).query();
		} catch (Exception e) {
			LogUtil.e("erro ao pergar atendimentos do alarme", e);
			return null;
		}finally{
			try {
				atendimentoDao().closeLastIterator();
			} catch (Exception e) {
				LogUtil.e("erro ao fechar cursor do objeto acesso", e);
			}
		}

	}

	public List<Comentario> getComentariosFromAlarme(Long id){
		try {
			return comentarioDao().queryBuilder().orderBy("id", false).where().eq("alarme_id", id).query();
		} catch (Exception e) {
			LogUtil.e("erro ao pergar atendimentos do alarme", e);
			return null;
		}finally{
			try {
				comentarioDao().closeLastIterator();
			} catch (Exception e) {
				LogUtil.e("erro ao fechar cursor do objeto acesso", e);
			}
		}

	}

	public Alarme getAlarmeFromDb(Long id){
		try {
			return  alarmeDao().queryForId(id);
		} catch (SQLException e) {
			LogUtil.e("erro ao pergar o alarme do banco de dados", e);
			return null;
		}
	}

	public List<Usuario> getUsuariosFromGrupoColaborativo(Usuario usuario){
		try {
			usuario = usuarioDao().queryForSameId(usuario);
			List<Usuario> toReturn = new ArrayList<Usuario>();
			for(GrupoUsuario grupoUsuario :usuario.getGrupoUsuarios()){
				List<Usuario> usuariosFromGrupoColaborativo = getUsuariosFromGrupoColaborativo(grupoUsuario.getGrupoColaborativo());
				for(Usuario usuario2 : usuariosFromGrupoColaborativo){
					if(!toReturn.contains(usuario2)){
						toReturn.add(usuario2); 					
					}
				}
			}
			return toReturn;
		} catch (SQLException e) {
			LogUtil.i("erro ao atualizar o usuario do banco", e);
			return null;
		}finally{
			try {
				usuarioDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar o ultimo iterador", e);
			}
		}


	}
	
	public List<Usuario> getUsuariosFromGrupoColaborativo(GrupoColaborativo grupoColaborativo) {
		if(usuarioPreparedQuery == null){
			try {
				usuarioPreparedQuery = makePreparedQueryForUsuario();
			} catch (SQLException e) {
				LogUtil.e("erro ao preparar query do usuaário", e);
				return Collections.emptyList();
			}
		}
		try {
			usuarioPreparedQuery.setArgumentHolderValue(0, grupoColaborativo);
			return usuarioDao().query(usuarioPreparedQuery);
		} catch (SQLException e) {
			LogUtil.i("errro ao buscar usuarios por grupo colaborarivo", e);
			return Collections.emptyList();
		}finally{
			try {
				usuarioDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("erro ao fechar ultimo iterador de usuarios", e);
			}
		}
	}


	private PreparedQuery<Usuario> makePreparedQueryForUsuario() throws SQLException {
		QueryBuilder<GrupoUsuario, Long> grupoUsuarioQB = grupoUsuarioDao().queryBuilder();
		grupoUsuarioQB.selectColumns(GrupoUsuario.USUARIO_ID_FIELD);
		SelectArg grupoSelectArg = new SelectArg();
		grupoUsuarioQB.where().eq(GrupoUsuario.GRUPO_ID_FIELD, grupoSelectArg);

		QueryBuilder<Usuario, Long> usuarioQueryBuilder = usuarioDao().queryBuilder();
		usuarioQueryBuilder.where().in("id", grupoUsuarioQB);

		return  usuarioQueryBuilder.prepare();
	}

	public List<Atendimento> findByEmail(String email,Alarme alarme){
		List<Atendimento> toReturn = null;
		try {
			QueryBuilder<Atendimento, Long> qb = atendimentoDao().queryBuilder();
			qb.where().eq("alarme_id", alarme.getId());
			toReturn =  qb.query();
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar atendimentos do usuário", e);
		}

		return toReturn;
	}


	public void deleteAll() throws SQLException{
		alarmeDao().deleteBuilder().delete();
		tipoSensorDao().deleteBuilder().delete();
		sensorDao().deleteBuilder().delete();
		usuarioDao().deleteBuilder().delete();
		atendimentoDao().deleteBuilder().delete();
		grupoColaborativoDao().deleteBuilder().delete();
		grupoUsuarioDao().deleteBuilder().delete();
		comentarioDao().deleteBuilder().delete();
	}

}
