package br.com.otgmobile.segurancacolaborativa.database;

import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.model.Atendimento;
import br.com.otgmobile.segurancacolaborativa.model.Comentario;
import br.com.otgmobile.segurancacolaborativa.model.GrupoColaborativo;
import br.com.otgmobile.segurancacolaborativa.model.GrupoUsuario;
import br.com.otgmobile.segurancacolaborativa.model.Sensor;
import br.com.otgmobile.segurancacolaborativa.model.TipoSensor;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

public class DatabaseConfigUtil extends OrmLiteConfigUtil {

	private static final Class<?>[] classes = new Class[] { Alarme.class,
			Sensor.class, TipoSensor.class, Atendimento.class, Usuario.class,
			Comentario.class, GrupoColaborativo.class,GrupoUsuario.class};

	public static void main(String[] args) throws Exception {
		writeConfigFile("ormlite_config.txt", classes);
	}
}