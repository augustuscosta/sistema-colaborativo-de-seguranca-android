package br.com.otgmobile.segurancacolaborativa.model;

import java.io.Serializable;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable
public class Sensor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1988813198155718145L;

	@DatabaseField(id=true)
	private Long id;
	
	@DatabaseField
	private String codigo;
	
	@DatabaseField
	@SerializedName("localizacao")
	private String locaizacao;
	
	@DatabaseField
	private String status;
	
	@DatabaseField
	private Double latitude;
	
	@DatabaseField
	private Double longitude;
	
	@SerializedName("tipo_sensor")
	@DatabaseField(foreign=true, foreignAutoCreate=true, foreignAutoRefresh=true,columnName="tipo_sensor")
	private TipoSensor tipoSensor;
	
	@SerializedName("usuario")
	@DatabaseField(foreign=true, foreignAutoCreate=true, foreignAutoRefresh=true,columnName="usuario")
	private Usuario usuario;
	
	@ForeignCollectionField
	private Collection<Alarme> alarmes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getLocaizacao() {
		return locaizacao;
	}

	public void setLocaizacao(String locaizacao) {
		this.locaizacao = locaizacao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Collection<Alarme> getAlarmes() {
		return alarmes;
	}

	public void setAlarmes(Collection<Alarme> alarmes) {
		this.alarmes = alarmes;
	}

	public TipoSensor getTipoSensor() {
		return tipoSensor;
	}

	public void setTipoSensor(TipoSensor tipoSensor) {
		this.tipoSensor = tipoSensor;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
