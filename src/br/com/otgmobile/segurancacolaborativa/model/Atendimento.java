package br.com.otgmobile.segurancacolaborativa.model;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Atendimento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7572482964706124360L;
	
	@DatabaseField(id=true)
	private Long id;
	
	@DatabaseField
	@SerializedName("hora_inicio")
	private Date inicio;
	
	@DatabaseField
	@SerializedName("hora_encerramento")
	private Date fim;
	
	@DatabaseField
	private String status;
	
	@DatabaseField(foreign=true, foreignAutoCreate=true, foreignAutoRefresh=true)
	private Usuario usuario;
	
	@DatabaseField(foreign=true)
	private Alarme alarme;
	
	@SerializedName("alarme_id")
	@DatabaseField(columnName="id_alarme")
	private Long alarmeId;

	@SerializedName("usuario_id")
	@DatabaseField(columnName="id_usuario")
	private Long usuarioID;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFim() {
		return fim;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Alarme getAlarme() {
		return alarme;
	}

	public void setAlarme(Alarme alarme) {
		this.alarme = alarme;
		if(alarme != null && alarme.getId()!= null){
			alarmeId = alarme.getId();
		}
	}

	public Long getAlarmeId() {
		return alarmeId;
	}

	public void setAlarmeId(Long alarmeId) {
		this.alarmeId = alarmeId;
	}

	public Long getUsuarioID() {
		return usuarioID;
	}

	public void setUsuarioID(Long usuarioID) {
		this.usuarioID = usuarioID;
	}


}
