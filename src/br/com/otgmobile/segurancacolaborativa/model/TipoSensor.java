package br.com.otgmobile.segurancacolaborativa.model;

import java.io.Serializable;
import java.util.Collection;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class TipoSensor implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3746331982677643319L;

	@DatabaseField(id=true)
	private Long id;
	
	@DatabaseField
	private String nome;
	
	@DatabaseField
	private String descricao;
	
	@ForeignCollectionField
	private Collection<Sensor> sensores;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Collection<Sensor> getSensores() {
		return sensores;
	}

	public void setSensores(Collection<Sensor> sensores) {
		this.sensores = sensores;
	}
}
