package br.com.otgmobile.segurancacolaborativa.model;

public enum AtendimentoStatus {

	indisponivel("indisponivel"),
	atendendo("atendendo"),
	atendido("atendido");

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	AtendimentoStatus(String value){
		this.value = value;
	}

	public static AtendimentoStatus fromValue(String value){
		for(AtendimentoStatus status : AtendimentoStatus.values()){
			if(status.getValue().equals(value)){
				return status;
			}
		}

		return null;
	}

}
