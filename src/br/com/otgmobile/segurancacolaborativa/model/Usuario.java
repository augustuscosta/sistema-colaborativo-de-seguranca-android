package br.com.otgmobile.segurancacolaborativa.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3433912390745055943L;

	@DatabaseField(id = true)
	private Long id;

	@DatabaseField
	private String email;

	@DatabaseField
	private String name;

	@DatabaseField
	private String nascimento;

	@DatabaseField
	private String sexo;

	@DatabaseField
	private String telefone;

	@DatabaseField
	private String celular;
	
	private String password;
	
	@SerializedName("password_confirmation")
	private String passwordConfirmation;

	@ForeignCollectionField
	private Collection<Atendimento> atendimento;

	@ForeignCollectionField
	private Collection<Sensor> sensores;

	@ForeignCollectionField
	private Collection<Comentario> comentarios;

	@ForeignCollectionField
	private Collection<GrupoUsuario> grupoUsuarios;

	@SerializedName("grupo_colaborativos")
	private List<GrupoColaborativo> gruposColaborativos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNascimento() {
		return nascimento;
	}

	public void setNascimento(String nascimento) {
		this.nascimento = nascimento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Collection<Sensor> getSensores() {
		return sensores;
	}

	public void setSensores(Collection<Sensor> sensores) {
		this.sensores = sensores;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public Collection<Atendimento> getAtendimento() {
		return atendimento;
	}

	public void setAtendimento(Collection<Atendimento> atendimento) {
		this.atendimento = atendimento;
	}

	public Collection<Comentario> getComentarios() {
		return comentarios;
	}

	public Collection<GrupoUsuario> getGrupoUsuarios() {
		return grupoUsuarios;
	}

	public void setGrupoUsuarios(Collection<GrupoUsuario> grupoUsuarios) {
		this.grupoUsuarios = grupoUsuarios;
	}

	public List<GrupoColaborativo> getGruposColaborativos() {
		return gruposColaborativos;
	}

	public void setGruposColaborativos(
			List<GrupoColaborativo> gruposColaborativos) {
		this.gruposColaborativos = gruposColaborativos;
	}

	public void setComentarios(Collection<Comentario> comentarios) {
		this.comentarios = comentarios;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
