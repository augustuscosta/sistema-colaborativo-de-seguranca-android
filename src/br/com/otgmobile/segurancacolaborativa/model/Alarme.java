package br.com.otgmobile.segurancacolaborativa.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable
public class Alarme implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6519597035598981961L;

	@DatabaseField(id=true)
	private Long id;
	
	@DatabaseField
	private Date data;

	@DatabaseField
	@SerializedName("tempo_atendimento")
	private Integer tempoAtendimento;
	
	@DatabaseField
	private String status;
	
	@DatabaseField
	private Double latitude;
	
	@DatabaseField
	private Double longitude;
	
	@SerializedName("sensor_id")	
	private Integer sensorId;
	
	@DatabaseField(foreign=true, foreignAutoCreate=true, foreignAutoRefresh=true,columnName="sensor")
	private Sensor sensor;
	
	@ForeignCollectionField()
	private Collection<Atendimento> atendimentos;
	
	@ForeignCollectionField()
	private  Collection<Comentario> comentarios;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Integer getTempoAtendimento() {
		return tempoAtendimento;
	}
	public void setTempoAtendimento(Integer tempoAtendimento) {
		this.tempoAtendimento = tempoAtendimento;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Integer getSensorId() {
		return sensorId;
	}
	public void setSensorId(Integer sensorId) {
		this.sensorId = sensorId;
	}
	public Sensor getSensor() {
		return sensor;
	}
	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}
	public Collection<Atendimento> getAtendimentos() {
		return atendimentos;
	}
	public void setAtendimentos(Collection<Atendimento> atendimentos) {
		this.atendimentos = atendimentos;
	}
	public Collection<Comentario> getComentarios() {
		return comentarios;
	}
	public void setComentarios(Collection<Comentario> comentarios) {
		this.comentarios = comentarios;
	}
	
	
}
