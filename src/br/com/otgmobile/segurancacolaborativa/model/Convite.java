package br.com.otgmobile.segurancacolaborativa.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable
public class Convite implements Serializable {

	private static final long serialVersionUID = 4191220463745247909L;

	@DatabaseField(id=true)
	private Long id;
	
	@DatabaseField
	@SerializedName("email_remetente")
	private String emailRemetente;
	
	@DatabaseField
	@SerializedName("email_destinatario")
	private String emailDestinatario;
	
	@DatabaseField
	@SerializedName("grupo_id")
	private Long grupoId;
	
	@DatabaseField
	@SerializedName("grupo_nome")
	private String grupoNome;
	
	@DatabaseField
	@SerializedName("nome_remetente")
	private String nomeRemetente;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmailRemetente() {
		return emailRemetente;
	}

	public void setEmailRemetente(String emailRemetente) {
		this.emailRemetente = emailRemetente;
	}

	public String getEmailDestinatario() {
		return emailDestinatario;
	}

	public void setEmailDestinatario(String emailDestinatario) {
		this.emailDestinatario = emailDestinatario;
	}

	public Long getGrupoId() {
		return grupoId;
	}

	public void setGrupoId(Long grupoId) {
		this.grupoId = grupoId;
	}

	public String getGrupoNome() {
		return grupoNome;
	}

	public void setGrupoNome(String grupoNome) {
		this.grupoNome = grupoNome;
	}

	public String getNomeRemetente() {
		return nomeRemetente;
	}

	public void setNomeRemetente(String nomeRemetente) {
		this.nomeRemetente = nomeRemetente;
	}
	
}
