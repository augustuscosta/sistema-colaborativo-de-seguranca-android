package br.com.otgmobile.segurancacolaborativa.model;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class GrupoUsuario implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4107877007931382559L;
	public static final String USUARIO_ID_FIELD ="usuario_id_field";
	public static final String GRUPO_ID_FIELD ="grupo_id_field";

	@DatabaseField(generatedId=true)
	private Long id;
	
	@DatabaseField(foreign=true,columnName=USUARIO_ID_FIELD)
	private Usuario usuario;
	
	@DatabaseField(foreign=true,columnName=GRUPO_ID_FIELD)
	private GrupoColaborativo grupoColaborativo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public GrupoColaborativo getGrupoColaborativo() {
		return grupoColaborativo;
	}

	public void setGrupoColaborativo(GrupoColaborativo grupoColaborativo) {
		this.grupoColaborativo = grupoColaborativo;
	}

}
