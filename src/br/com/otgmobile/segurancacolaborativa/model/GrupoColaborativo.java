package br.com.otgmobile.segurancacolaborativa.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class GrupoColaborativo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5864245699019368113L;
	
	@DatabaseField(id=true)
	private Long id;
	
	@DatabaseField
	private String nome;
	
	@DatabaseField
	private String descricao;
	
	@DatabaseField
	@SerializedName("email_dono")
	private String emailDono;
	
	@ForeignCollectionField
	private Collection<GrupoUsuario> grupoUsuarios;
	
	private List<Usuario> usuarios;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Collection<GrupoUsuario> getGrupoUsuarios() {
		return grupoUsuarios;
	}

	public void setGrupoUsuarios(Collection<GrupoUsuario> grupoUsuarios) {
		this.grupoUsuarios = grupoUsuarios;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public String getEmailDono() {
		return emailDono;
	}

	public void setEmailDono(String emailDono) {
		this.emailDono = emailDono;
	}

}
