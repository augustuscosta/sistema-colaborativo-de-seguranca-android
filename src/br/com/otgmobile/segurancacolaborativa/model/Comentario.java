package br.com.otgmobile.segurancacolaborativa.model;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable
public class Comentario implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3944393366543629813L;

	@DatabaseField(id=true)
	private Long id;
	
	@DatabaseField
	@SerializedName("data")
	private Date date;
	
	@DatabaseField
	private String mensagem;
	
	@DatabaseField(foreign=true, foreignAutoCreate=true,foreignAutoRefresh=true)
	private Usuario usuario;
	
	@DatabaseField(foreign=true, foreignAutoCreate=true,foreignAutoRefresh=true)
	private Alarme alarme;
	
	//Transient parameter for rails not stored on database
	@SerializedName("alarme_id")
	@DatabaseField(columnName="id_alarme")
	private Long alarmId;
	
	
	@SerializedName("usuario_id")
	@DatabaseField(columnName="id_usuario")
	private Long usuarioID;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Alarme getAlarme() {
		return alarme;
	}

	public void setAlarme(Alarme alarme) {
		this.alarme = alarme;
		if(alarme != null && alarme.getId() != null){
			alarmId = alarme.getId() ;
		}
	}

	public Long getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(Long alarmId) {
		this.alarmId = alarmId;
	}

	public Long getUsuarioID() {
		return usuarioID;
	}

	public void setUsuarioID(Long usuarioID) {
		this.usuarioID = usuarioID;
	}

}
