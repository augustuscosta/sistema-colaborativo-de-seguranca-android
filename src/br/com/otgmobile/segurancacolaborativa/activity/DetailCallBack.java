package br.com.otgmobile.segurancacolaborativa.activity;

public interface DetailCallBack {
	
	void viewMap();
	
	void comment();

}
