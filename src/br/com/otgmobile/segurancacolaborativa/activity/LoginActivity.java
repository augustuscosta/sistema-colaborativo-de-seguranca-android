package br.com.otgmobile.segurancacolaborativa.activity;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.cloud.RestClient;
import br.com.otgmobile.segurancacolaborativa.cloud.SessionCloud;
import br.com.otgmobile.segurancacolaborativa.util.AppHelper;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;
import br.com.otgmobile.segurancacolaborativa.util.Session;

import com.crittercism.app.Crittercism;

public class LoginActivity extends RoboActivity {
    
    private UserLoginTask mAuthTask = null;

    // Values for email and password at the time of the login attempt.
    private String mEmail;
    private String mPassword;

    // UI references.
    @InjectView(R.id.email) private EditText mEmailView;
    @InjectView(R.id.password)  private EditText mPasswordView;
    @InjectView(R.id.login_form) private View mLoginFormView;
    @InjectView(R.id.login_status) private View mLoginStatusView;
    @InjectView(R.id.login_status_message) private TextView mLoginStatusMessageView;
    @InjectView(R.id.sign_in_button) private Button mSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        LogUtil.initLog(this, "SISTEMA COLABORATIVO DE SEGURANÇA");
        Crittercism.init(getApplicationContext(), "51d81d0ea7928a7a4d000002");
        mEmail="";
        mPassword="";
        
       
        startActivity(new Intent(this, SplashScreen.class));
        mEmailView.setText(mEmail);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        mPasswordView.setText(mPassword);
        
    }
    
    @Override
    public void onResume(){
    	super.onResume();
    	mPassword="";
    	mPasswordView.setText(mPassword);
    	checkToken();
    }
    
    private void checkToken(){
    	 if(Session.getToken(this) != null){
    		 hideKeyboard();
         	startActivity(new Intent(this, MainActivity.class));
         }
    }
    
    private void hideKeyboard(){
    	InputMethodManager imm = (InputMethodManager)getSystemService(
			      Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mPasswordView.getWindowToken(), 0);
    }
    
    public void signIn(View view){
    	signIn();
    }
    
    private void signIn(){
    	startActivity(new Intent(this, SigninActivity.class));
    }
    
    public void attemptLogin(View view){
    	attemptLogin();
    }
    
    public void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        mEmail = mEmailView.getText().toString();
        mPassword = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password.
        if (TextUtils.isEmpty(mPassword)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (mPassword.length() < 8) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(mEmail)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!mEmail.contains("@")) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
            showProgress(true);
            mAuthTask = new UserLoginTask(this);
            mAuthTask.execute();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginStatusView.setVisibility(View.VISIBLE);
            mLoginStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });

            mLoginFormView.setVisibility(View.VISIBLE);
            mLoginFormView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<String, Void, RestClient> {
    	Context context;
    	SessionCloud cloud;
    	String erroMessage = null;
    	
    	UserLoginTask(Context context) {
			this.context = context;
		}
    	
        @Override
        protected RestClient doInBackground(String... params) {
            return login();
        }
        
        RestClient login(){
			cloud = new SessionCloud(context);
			try {
				cloud.login(mEmailView.getText().toString(), mPasswordView.getText().toString());
			} catch (Throwable e) {
				erroMessage = e.getLocalizedMessage();
				return cloud;
			}
			if(cloud.getResponseCode() != 201){
				erroMessage = cloud.getErrorMessage();
				return cloud;
			}
			return cloud;
		}

        @Override
        protected void onPostExecute(final RestClient cloud) {
            mAuthTask = null;
            showProgress(false);
            
            if (cloud.getResponseCode() != 201) {
				if(erroMessage == null){
					AppHelper.getInstance().presentError(context,"Erro na conexão: ","Response code: " + cloud.getResponseCode());
					mPasswordView.setError(getString(R.string.error_incorrect_password));
	                mPasswordView.requestFocus();
				}else{
					AppHelper.getInstance().presentError(context,"Erro na conexão: ",erroMessage);
				}
			} else {
				Session.setEmailToPreferences(context, mEmail);
				hideKeyboard();
				startActivity(new Intent(context,MainActivity.class));
			}

        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }

    }
    
}
