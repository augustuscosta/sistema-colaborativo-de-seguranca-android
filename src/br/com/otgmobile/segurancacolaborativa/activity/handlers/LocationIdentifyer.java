package br.com.otgmobile.segurancacolaborativa.activity.handlers;

import android.location.Location;

public interface LocationIdentifyer {
	
	Location getLastLocation();

}
