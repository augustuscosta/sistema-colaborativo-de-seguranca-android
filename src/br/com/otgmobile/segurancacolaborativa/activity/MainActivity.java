package br.com.otgmobile.segurancacolaborativa.activity;

import java.lang.ref.WeakReference;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.activity.handlers.LocationIdentifyer;
import br.com.otgmobile.segurancacolaborativa.cloud.AlarmeCloud;
import br.com.otgmobile.segurancacolaborativa.cloud.RegistrationIdCloud;
import br.com.otgmobile.segurancacolaborativa.database.DatabaseAccessProvider;
import br.com.otgmobile.segurancacolaborativa.fragments.AlarmeListFragment;
import br.com.otgmobile.segurancacolaborativa.fragments.CreateAlarmeFragment;
import br.com.otgmobile.segurancacolaborativa.gps.DLGps;
import br.com.otgmobile.segurancacolaborativa.gps.DLGpsObserver;
import br.com.otgmobile.segurancacolaborativa.model.Alarme;
import br.com.otgmobile.segurancacolaborativa.util.AppHelper;
import br.com.otgmobile.segurancacolaborativa.util.LogUtil;
import br.com.otgmobile.segurancacolaborativa.util.Session;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class MainActivity extends SherlockFragmentActivity implements LocationIdentifyer{


	ViewPager mViewPager;
	TabsAdapter mTabsAdapter;
	TextView tabCenter;
	TextView tabText;
	ActionBar bar;
	private Location mLocation;
	private GPSThread thread;
	private GoogleCloudMessaging gcm;
	private static GetAllAlarmsTask getAllAlarmsTask;
	private CreateAlarmeTask createAlarmeTask;
	List<WeakReference<Fragment>> fragList = new ArrayList<WeakReference<Fragment>>();
	private DatabaseAccessProvider provider;
	private AlarmeCloud alarmeCloud;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		startGCMandViewPager();
		setContentView(mViewPager);
		instantiateActionBar();
		new RegisterTask(this).execute();
	}

	private void startGCMandViewPager() {
		gcm = GoogleCloudMessaging.getInstance(this);
		mViewPager = new ViewPager(this);
		mViewPager.setId(R.id.pager);
	}

	private void instantiateActionBar() {
		bar = getSupportActionBar();
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		setTabsAdapterAndFillTabs();
	}

	private void setTabsAdapterAndFillTabs() {
		mTabsAdapter = new TabsAdapter(this, mViewPager);
		fillTabs();
	}

	private void fillTabs() {
		mTabsAdapter.addTab(bar.newTab().setText(R.string.acionar_alarme),
				CreateAlarmeFragment.class, null);
		mTabsAdapter.addTab(bar.newTab().setText(R.string.alarmes),
				AlarmeListFragment.class, null);
	}

	@Override
	protected void onResume() {
		super.onResume();
		startTGpsThread();
	}

	private void startTGpsThread() {
		if(thread == null){
			thread = new GPSThread();
		}
		thread.start();
	}
	

	@Override
	public void onBackPressed() {
		askForLogout();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		com.actionbarsherlock.view.MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.activity_main, (com.actionbarsherlock.view.Menu) menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.grupos:
			openGroupsControl();
			break;
		case R.id.refresh:
			executeGetAlarmesTask();
			break;
		case R.id.menu_logout:
			askForLogout();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void openGroupsControl(){
		Intent intent = new Intent(this,GroupsActivity.class);
		startActivity(intent);
	}

	private void executeGetAlarmesTask() {
		mViewPager.setCurrentItem( 1, true);		
		stopAlarmesTask();
		if(getAllAlarmsTask == null ){
			getAllAlarmsTask = new GetAllAlarmsTask(this);
		}

		getAllAlarmsTask.execute();
	}

	private void stopAlarmesTask() {
		if(getAllAlarmsTask != null ){
			getAllAlarmsTask.cancel(true);
		}

		getAllAlarmsTask = null;
	}

	@Override
	protected void onPause() {
		stopAlarmesTask();
		stopThread();
		super.onPause();
	}

	private void stopThread() {
		if(thread != null){
			thread.interrupt();
			thread = null;
		}
	}

	@Override
	public void onAttachFragment(Fragment fragment) {
		fragList.add(new WeakReference<Fragment>(fragment));
		super.onAttachFragment(fragment);
	}

	private void askForLogout(){
		new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setTitle(R.string.logout)
		.setMessage(R.string.logout_sure)
		.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				new UnRegisterTask(MainActivity.this).execute();  
			}

		})
		.setNegativeButton(R.string.no, null)
		.show();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}


	class RegisterTask extends AsyncTask<String, Void, String> {
		ProgressDialog mDialog;
		Context context;

		RegistrationIdCloud cloud;


		RegisterTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "", getString(R.string.SERVER_REGISTER), false, true);
			cloud = new RegistrationIdCloud(context);
		}

		@Override
		public String doInBackground(String... params){
			return register();
		}

		String register(){
			try {
				if(Session.getRegistrationId(context) == null)
					Session.setRegistrationId(gcm.register(Session.getSenderId(context)), context);

				if (!Session.getServerRegistred(context)) {
					cloud.register(Session.getRegistrationId(context));
					if (cloud.getResponseCode() != 200) {
						String erro = cloud.getErrorMessage();
						if(erro == null){
							erro = cloud.getResponseCode() + "";
						}
						return erro;
					} else {
						Session.setServerRegistred(true, context);
					}

				}

			} catch (Throwable e) {
				return e.getLocalizedMessage();
			}


			return null;
		}

		@Override
		public void onPostExecute(String erroMessage) {
			mDialog.dismiss();
			if (erroMessage != null) {
				AppHelper.getInstance().presentError(context,getResources().getString(R.string.CONNECTION_ERROR),erroMessage);
			}else{
				Toast.makeText(context, context.getString(R.string.REGISTRED), Toast.LENGTH_LONG).show();
			}

		}

	}

	class UnRegisterTask extends AsyncTask<String, Void, String> {
		ProgressDialog mDialog;
		Context context;

		RegistrationIdCloud cloud;


		UnRegisterTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "", getString(R.string.SERVER_REGISTER), false, true);
			cloud = new RegistrationIdCloud(context);
		}

		@Override
		public String doInBackground(String... params){
			return unregister();
		}

		String unregister(){
			try {
				if (Session.getServerRegistred(context)) {
					cloud.unregister();
					if (cloud.getResponseCode() != 200) {
						String erro = cloud.getErrorMessage();
						if(erro == null){
							erro = cloud.getResponseCode() + "";
						}
						return erro;
					}

				}

			} catch (Throwable e) {
				return e.getLocalizedMessage();
			}


			return null;
		}

		@Override
		public void onPostExecute(String erroMessage) {
			mDialog.dismiss();
			if (erroMessage != null) {
				AppHelper.getInstance().presentError(context,getResources().getString(R.string.CONNECTION_ERROR),erroMessage);
			}else{
				Session.setServerRegistred(false, context);
				Session.setToken(null, context);
				try {
					provider().deleteAll();
				} catch (SQLException e) {
					LogUtil.e("erro ao limpar banco de dados", e);
				}
				finish();
			}

		}

	}
	class GetAllAlarmsTask extends AsyncTask<String, Void, String> {
		ProgressDialog mDialog;
		Context context;

		AlarmeCloud cloud;


		GetAllAlarmsTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "", context.getString(R.string.buscando_os_ultimos_alarmes), false, true);
			cloud = new AlarmeCloud(context);
		}

		@Override
		public String doInBackground(String... params){
			return fetchAlarmes();
		}

		String fetchAlarmes(){
			try {
				cloud.getAllAlarmes();
				if (cloud.getResponseCode() != 200) {
					String erro = cloud.getErrorMessage();
					if(erro == null){
						erro = cloud.getResponseCode() + "";
					}
					return erro;
				}
			} catch (Throwable e) {
				return e.getLocalizedMessage();
			}
			return null;
		}

		@Override
		public void onPostExecute(String erroMessage) {
			mDialog.dismiss();
			if (erroMessage != null) {
				AppHelper.getInstance().presentError(context,getResources().getString(R.string.CONNECTION_ERROR),erroMessage);
			}else{
				setFragmentAndPopulateList();
			}

		}

	}

	private void setFragmentAndPopulateList() {
		AlarmeListFragment fragment = getActiveAlarmeListFragment();
		if(fragment != null){
			if(mViewPager.getCurrentItem() != 1){
				mViewPager.setCurrentItem( 1, true);				
				fragment.populateAlarmes();
			}else{
				fragment.populateAlarmes();
			}
		}
	}

	public DatabaseAccessProvider provider() {
		if(provider == null){
			provider = new DatabaseAccessProvider(this);
		}
		return provider;
	}


	public static class TabsAdapter extends FragmentPagerAdapter implements
	ActionBar.TabListener, ViewPager.OnPageChangeListener {
		private final Context mContext;
		private final ActionBar mActionBar;
		private final ViewPager mViewPager;
		protected final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();


		static final class TabInfo {
			private final Class<?> clss;
			private final Bundle args;

			TabInfo(Class<?> _class, Bundle _args) {
				clss = _class;
				args = _args;
			}
		}

		public TabsAdapter(SherlockFragmentActivity activity, ViewPager pager) {
			super(activity.getSupportFragmentManager());
			mContext = activity;
			mActionBar = activity.getSupportActionBar();
			mViewPager = pager;
			mViewPager.setAdapter(this);
			mViewPager.setOnPageChangeListener(this);
		}

		public void addTab(ActionBar.Tab tab, Class<?> clss, Bundle args) {
			TabInfo info = new TabInfo(clss, args);
			tab.setTag(info);
			tab.setTabListener(this);
			mTabs.add(info);
			mActionBar.addTab(tab);
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return mTabs.size();
		}

		@Override
		public Fragment getItem(int position) {
			TabInfo info = mTabs.get(position);
			return Fragment.instantiate(mContext, info.clss.getName(),
					info.args);
		}

		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {
		}

		public void onPageSelected(int position) {
			mActionBar.setSelectedNavigationItem(position);
		}

		public void onPageScrollStateChanged(int state) {
		}

		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			Object tag = tab.getTag();
			for (int i = 0; i < mTabs.size(); i++) {
				if (mTabs.get(i) == tag) {
					mViewPager.setCurrentItem(i);
				}
			}
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}


	public void presentError(Context context, String title, String description) {
		AlertDialog.Builder d = new AlertDialog.Builder(context);
		d.setTitle(title);
		d.setMessage(description);
		d.setIcon(android.R.drawable.ic_dialog_alert);
		d.setNeutralButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				finish();
			}
		});
		d.show();
	}


	public AlarmeListFragment getActiveAlarmeListFragment() {
		for(WeakReference<Fragment> ref : fragList) {
			if(ref.get()!= null &&ref.get().getClass().equals(AlarmeListFragment.class)&& !ref.get().isDetached()) {
				return (AlarmeListFragment) ref.get();
			}
		}
		return null;
	}
	public CreateAlarmeFragment getActiveCreateAlarmeFragment() {
		for(WeakReference<Fragment> ref : fragList) {
			if(ref.get()!= null &&ref.get().getClass().equals(CreateAlarmeFragment.class)&& !ref.get().isDetached()) {
				return (CreateAlarmeFragment) ref.get();
			}
		}
		return null;
	}

	private class GPSThread extends Thread {

		@Override
		public void run() {
			try {
				Looper.prepare();
				DLGps.addGpsObserver(gpsObserver, getApplicationContext());
				Looper.loop();
			} finally {
				Looper.myLooper().quit();
			}
		}
	}
	
	
	class CreateAlarmeTask extends AsyncTask<Void, Void,Void> {
 		ProgressDialog mDialog;
 		Context context;
 		
 		Alarme alarme;
 		int ok = 0;

 		CreateAlarmeTask(Context context, Alarme alarme) {
 			this.context = context;
 			this.alarme = alarme;
 			mDialog = ProgressDialog.show(context, "", context.getString(R.string.criando_alarme), false, true);
 		}


		@Override
		protected Void doInBackground(Void... params) {
			createAlarmeOnCloud();
			return null;
		}
		
		private void createAlarmeOnCloud() {
			try {
				ok = alarmeCloud().createAlarme(alarme);
			} catch (Exception e) {
				LogUtil.e("Erro ao criar alarme", e);
				AppHelper.getInstance().presentError(context, context.getString(R.string.erro_ao_criar_alarme), context.getString(R.string.falha_de_conexao)+e.getMessage());
			}
			
			
			
		}


		@Override
		protected void onPostExecute(Void result) {
			mDialog.dismiss();
			mDialog = null;
			if(ok != HttpStatus.SC_CREATED){
				AppHelper.getInstance().presentError(context, context.getString(R.string.erro_ao_criar_alarme), context.getString(R.string.falha_de_conexao));
			}else{
				AppHelper.getInstance().presentError(context, context.getString(R.string.alarme_criado_com_sucesso), context.getString(R.string.seu_alarme_foi_enviado_com_sucesso_ao_servidor));				
			}
			stopCreateAlarmeTask();
			super.onPostExecute(result);
		}


 	}
	
	private void executeCreateAlarmeTask(Alarme alarme) {
		if(createAlarmeTask == null){
			createAlarmeTask =new CreateAlarmeTask(this, alarme);
			
		}
		createAlarmeTask.execute();
	}
	private void stopCreateAlarmeTask() {
		if(createAlarmeTask != null){
			createAlarmeTask.cancel(true);
			
		}
		createAlarmeTask = null;
	}
	
	
	private AlarmeCloud alarmeCloud(){
		if(alarmeCloud == null){
			alarmeCloud = new AlarmeCloud(this);
		}
		return alarmeCloud;
	}


	final private DLGpsObserver gpsObserver = new DLGpsObserver() {

		@Override
		public void onStatusChanged(int status) {
			if ( LocationProvider.AVAILABLE != status ) { 
				mLocation = null;
			}
		}

		@Override
		public void onLocationChanged(Location location) {
			mLocation = location;

		}
	};

	@Override
	public Location getLastLocation() {
		return mLocation;
	}
	
	public void onClickCreateAlarme(View view){
		createNewAlarme();
	}
	
	
	private void createNewAlarme() {
		Alarme alarme = new Alarme();
		alarme.setData(AppHelper.getCurrentDate());
		if(getLastLocation()!=null){
			alarme.setLatitude(getLastLocation().getLatitude());
			alarme.setLongitude(getLastLocation().getLongitude());
			executeCreateAlarmeTask(alarme);			
		}else{
			createConFinrDialaog(alarme);
		}
		
	}

	private void createConFinrDialaog(final Alarme alarme) {
		AlertDialog.Builder d = new AlertDialog.Builder(this);
		d.setTitle(R.string.sem_localiza_o);
		d.setMessage(R.string.n_o_consiguimos_obter_sua_localiza_o_deseja_criar_o_alarme);
		d.setIcon(android.R.drawable.ic_dialog_alert);
		d.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {				
				executeCreateAlarmeTask(alarme);			
			}	
		});
		d.setNeutralButton(getString(R.string.no), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			}	
		});
		
		d.show();
	}
}
