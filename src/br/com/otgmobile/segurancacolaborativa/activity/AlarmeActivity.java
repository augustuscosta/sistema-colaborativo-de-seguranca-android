package br.com.otgmobile.segurancacolaborativa.activity;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.fragments.AlarmeDetailFragment;
import br.com.otgmobile.segurancacolaborativa.fragments.AlarmeMapFragment;
import br.com.otgmobile.segurancacolaborativa.fragments.ComentarioListFragment;
import br.com.otgmobile.segurancacolaborativa.util.ConstUtil;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class AlarmeActivity extends SherlockFragmentActivity implements DetailCallBack{

	
	
	ViewPager mViewPager;
	TabsAdapter mTabsAdapter;
	TextView tabCenter;
	TextView tabText;
	ActionBar bar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mViewPager = new ViewPager(this);
		mViewPager.setId(R.id.pager);
		startGCMandViewPager();
		setContentView(mViewPager);
		instantiateActionBar();
		handleIntent();
	}
	
	private void handleIntent() {
		Intent intent = getIntent();
		int page = intent.getIntExtra(ConstUtil.PAGE, 0);
		mViewPager.setCurrentItem(page);
	}

	private void startGCMandViewPager() {
		mViewPager = new ViewPager(this);
		mViewPager.setId(R.id.pager);
	}
	
	private void instantiateActionBar() {
		bar = getSupportActionBar();
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		setTabsAdapterAndFillTabs();
	}

	private void setTabsAdapterAndFillTabs() {
		mTabsAdapter = new TabsAdapter(this, mViewPager);
		fillTabs();
	}
	
	private void fillTabs() {
		Intent intent = getIntent();
		mTabsAdapter.addTab(bar.newTab().setText(getString(R.string.detalhes)),
				AlarmeDetailFragment.class, intent.getExtras());
		mTabsAdapter.addTab(bar.newTab().setText(getString(R.string.localizacao)),
				AlarmeMapFragment.class, intent.getExtras());
		mTabsAdapter.addTab(bar.newTab().setText(getString(R.string.comentarios)),
				ComentarioListFragment.class, intent.getExtras());
		
	}

	public static class TabsAdapter extends FragmentPagerAdapter implements
	ActionBar.TabListener, ViewPager.OnPageChangeListener {
		private final Context mContext;
		private final ActionBar mActionBar;
		private final ViewPager mViewPager;
		private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();
		

		static final class TabInfo {
			private final Class<?> clss;
			private final Bundle args;

			TabInfo(Class<?> _class, Bundle _args) {
				clss = _class;
				args = _args;
			}
		}

		public TabsAdapter(SherlockFragmentActivity activity, ViewPager pager) {
			super(activity.getSupportFragmentManager());
			mContext = activity;
			mActionBar = activity.getSupportActionBar();
			mViewPager = pager;
			mViewPager.setAdapter(this);
			mViewPager.setOnPageChangeListener(this);
		}

		public void addTab(ActionBar.Tab tab, Class<?> clss, Bundle args) {
			TabInfo info = new TabInfo(clss, args);
			tab.setTag(info);
			tab.setTabListener(this);
			mTabs.add(info);
			mActionBar.addTab(tab);
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return mTabs.size();
		}

		@Override
		public Fragment getItem(int position) {
			TabInfo info = mTabs.get(position);
			return Fragment.instantiate(mContext, info.clss.getName(),
					info.args);
		}

		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {
		}

		public void onPageSelected(int position) {
			mActionBar.setSelectedNavigationItem(position);
		}

		public void onPageScrollStateChanged(int state) {
		}

		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			Object tag = tab.getTag();
			for (int i = 0; i < mTabs.size(); i++) {
				if (mTabs.get(i) == tag) {
					mViewPager.setCurrentItem(i);
				}
			}
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}

	@Override
	public void viewMap() {
		mViewPager.setCurrentItem(1);		
	}

	@Override
	public void comment() {
		mViewPager.setCurrentItem(2);		
		
	}
	
	public void onClickMap(View view){
		viewMap();
	}
	
	public void onClickComment(View view){
		viewMap();
	}
	
	


}
