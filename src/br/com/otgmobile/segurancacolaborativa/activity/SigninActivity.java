package br.com.otgmobile.segurancacolaborativa.activity;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import br.com.otgmobile.segurancacolaborativa.R;
import br.com.otgmobile.segurancacolaborativa.cloud.RegistrationCloud;
import br.com.otgmobile.segurancacolaborativa.cloud.RestClient;
import br.com.otgmobile.segurancacolaborativa.cloud.SessionCloud;
import br.com.otgmobile.segurancacolaborativa.model.Usuario;
import br.com.otgmobile.segurancacolaborativa.util.AppHelper;
import br.com.otgmobile.segurancacolaborativa.util.Session;

public class SigninActivity extends RoboActivity {
    
    private UserSigninTask mAuthTask = null;

    // Values for email and password at the time of the login attempt.
    private String mName;
    private String mEmail;
    private String mTelefone;
    private String mPassword;
    private String mConfirmPassword;

    // UI references.
    @InjectView(R.id.name) private EditText mNameView;
    @InjectView(R.id.email) private EditText mEmailView;
    @InjectView(R.id.telefone) private EditText mTelefoneView;
    @InjectView(R.id.password)  private EditText mPasswordView;
    @InjectView(R.id.confirm_password)  private EditText mConfirmPasswordView;
    @InjectView(R.id.signin_form) private View mSigninFormView;
    @InjectView(R.id.signin_status) private View mSigninStatusView;
    @InjectView(R.id.signin_status_message) private TextView mSigninStatusMessageView;
    @InjectView(R.id.sign_in_button) private Button mSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        mName = "";
        mEmail="";
        mTelefone="";
        mPassword="";
        mConfirmPassword = "";
        
        mNameView.setText(mName);
        mEmailView.setText(mEmail);
        mTelefoneView.setText(mTelefone);
        mPasswordView.setText(mPassword);
        mConfirmPasswordView.setText(mConfirmPassword);
        mConfirmPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptSignin();
                    return true;
                }
                return false;
            }
        });
        
    }
    
    public void signIn(View view){
    	signIn();
    }
    
    private void signIn(){
    	
    }
    
    public void attemptSignin(View view){
    	attemptSignin();
    }
    
    public void attemptSignin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mNameView.setError(null);
        mEmailView.setError(null);
        mTelefoneView.setError(null);
        mPasswordView.setError(null);
        mConfirmPasswordView.setError(null);

        // Store values at the time of the login attempt.
        mName = mNameView.getText().toString();
        mEmail = mEmailView.getText().toString();
        mTelefone = mTelefoneView.getText().toString();
        mPassword = mPasswordView.getText().toString();
        mConfirmPassword = mConfirmPasswordView.getText().toString();

        if (!isValid()) {
            mSigninStatusMessageView.setText(R.string.login_progress_signing_in);
            showProgress(true);
            mAuthTask = new UserSigninTask(this);
            mAuthTask.execute();
        }
    }
    
    private Boolean isValid(){
    	
    	// Check for a valid name.
        if (TextUtils.isEmpty(mName)) {
            mNameView.setError(getString(R.string.error_field_required));
            mNameView.requestFocus();
            return true;
        }
        
     // Check for a valid telefone.
        if (TextUtils.isEmpty(mTelefone)) {
        	mTelefoneView.setError(getString(R.string.error_field_required));
        	mTelefoneView.requestFocus();
            return true;
        }
    	
    	// Check for a valid email address.
        if (TextUtils.isEmpty(mEmail)) {
            mEmailView.setError(getString(R.string.error_field_required));
            mEmailView.requestFocus();
            return true;
        } else if (!mEmail.contains("@")) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            mEmailView.requestFocus();
            return true;
        }
    	
    	// Check for a valid password.
        if (TextUtils.isEmpty(mPassword)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            mEmailView.requestFocus();
            return true;
        } else if (mPassword.length() < 4) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            mPasswordView.requestFocus();
            return true;
        }
        
        // Check for a valid confirm password.
        if (TextUtils.isEmpty(mConfirmPassword)) {
        	mConfirmPasswordView.setError(getString(R.string.error_field_required));
        	mConfirmPasswordView.requestFocus();
            return true;
        } else if (mConfirmPassword.length() < 8) {
        	mConfirmPasswordView.setError(getString(R.string.error_invalid_password));
        	mConfirmPasswordView.requestFocus();
            return true;
        }
        
        // Check for a valid confirm password.
        if (!mPassword.equals(mConfirmPassword)) {
        	mConfirmPasswordView.setError(getString(R.string.error_passwords_not_equal));
        	mConfirmPasswordView.requestFocus();
            return true;
        }

        return false;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mSigninStatusView.setVisibility(View.VISIBLE);
            mSigninStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	mSigninStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });

            mSigninStatusView.setVisibility(View.VISIBLE);
            mSigninStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mSigninFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
        	mSigninStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
        	mSigninFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserSigninTask extends AsyncTask<String, Void, RestClient> {
    	Context context;
    	RegistrationCloud registrationCloud;
    	SessionCloud sessionCloud;
    	String erroMessage = null;
    	
    	UserSigninTask(Context context) {
			this.context = context;
		}
    	
        @Override
        protected RestClient doInBackground(String... params) {
            return signIn();
        }
        
        RestClient signIn(){
        	//CREATE USER
        	registrationCloud = new RegistrationCloud(context);
			try {
				Usuario user = new Usuario();
				user.setName(mName);
				user.setEmail(mEmail);
				user.setTelefone(mTelefone);
				user.setPassword(mPassword);
				user.setPasswordConfirmation(mConfirmPassword);
				
				registrationCloud.createUsuario(user);
				
			} catch (Throwable e) {
				erroMessage = e.getLocalizedMessage();
				return registrationCloud;
			}
			if(registrationCloud.getResponseCode() != 201){
				erroMessage = registrationCloud.getErrorMessage();
				return registrationCloud;
			}
        	
        	
        	//SESSION
			sessionCloud = new SessionCloud(context);
			try {
				sessionCloud.login(mEmail, mPassword);
			} catch (Throwable e) {
				erroMessage = e.getLocalizedMessage();
				return sessionCloud;
			}
			if(sessionCloud.getResponseCode() != 201){
				erroMessage = sessionCloud.getErrorMessage();
				return sessionCloud;
			}
			return sessionCloud;
		}

        @Override
        protected void onPostExecute(final RestClient cloud) {
            mAuthTask = null;
            showProgress(false);
            
            if (cloud.getResponseCode() != 201) {
				if(erroMessage == null){
					AppHelper.getInstance().presentError(context,"Erro na conexão: ","Response code: " + cloud.getResponseCode());
				}else{
					AppHelper.getInstance().presentError(context,"Erro na conexão: ",erroMessage);
				}
			} else {
				Session.setEmailToPreferences(context, mEmail);
				InputMethodManager imm = (InputMethodManager)getSystemService(
	    			      Context.INPUT_METHOD_SERVICE);
	    		imm.hideSoftInputFromWindow(mConfirmPasswordView.getWindowToken(), 0);
				finish();
			}

        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }

    }
    
}
